# Věci co jsou potřebný pro běh
- [Microsoft® ODBC Driver 11 for SQL Server®](https://www.microsoft.com/en-us/download/details.aspx?id=36434)
- PHP5.6 pdo_odbc
- [Microsoft Drivers for PHP for SQL Server](https://www.microsoft.com/en-us/download/details.aspx?id=20098) verze 3.2

# Před předáním
- Vyresetovat auto incremet u tabulky typ_vzorku... maj to všude dělaný podle ID a je to prcárna, zrovna tuto kdyby měli podle názvu tak je to pohoda :(
``` SQL
DBCC CHECKIDENT (typ_vzorku, RESEED, 0)
```
