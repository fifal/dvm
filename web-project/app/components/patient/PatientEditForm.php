<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 2. 7. 2018
 * Time: 15:47
 */
namespace App\Components;

use App\Model\PatientModel;
use Nette\Application\UI\Form;

class PatientEditForm extends \Nette\Application\UI\Control
{
    /**
     * @var PatientModel
     */
    private $patientModel;

    /**
     * PatientEditForm constructor.
     * @param $patientModel
     */
    public function __construct(PatientModel $patientModel)
    {
        parent::__construct();
        $this->patientModel = $patientModel;
    }

    /**
     * Creates new form for adding patients
     *
     * @return \Nette\Application\UI\Form
     */
    public function create(){
        $form = new BootstrapForm;


        $form->addText('pacient_jmeno', 'Jméno')
            ->addRule(Form::FILLED, 'Musíte vyplnit jméno pacienta');

        $form->addText('pacient_prijmeni', 'Příjmení')
            ->addRule(Form::FILLED, 'Musíte vyplnit příjmení pacienta');

        $form->addText('pacient_rc', 'Rodné číslo (bez lomítka)')
            ->setHtmlAttribute('placeholder', '9901011234')
            ->addRule(Form::FILLED, 'Musíte vyplnit RČ pacienta')
            ->addRule(Form::LENGTH, 'Délka rodného čísla musí být %d.', 10);

        return $form;
    }
}