<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 20.06.2018
 * Time: 20:19
 */
namespace App\Components;

use App\Model\EmployeeModel;
use Nette\Application\UI\Form;

class UserPasswordForm extends \Nette\Application\UI\Control
{
    /** @var EmployeeModel */
    private $employeeModel;

    public function __construct(EmployeeModel $employeeModel)
    {
        parent::__construct();
        $this->employeeModel = $employeeModel;
    }

    /**
     * Creates new form for user edit
     *
     * @return \Nette\Application\UI\Form
     */
    public function create(){
        $form = new BootstrapForm;

        $form->addPassword('password', 'Heslo')
            ->setRequired(true)
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít minimálně 4 znaky!', 4);

        $form->addPassword('passwordConfirm', 'Heslo znovu')
            ->setRequired(true)
            ->addRule(Form::EQUAL, 'Hesla se neshodují!', $form['password']);

        return $form;
    }

}