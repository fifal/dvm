<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 27.08.2018
 * Time: 20:59
 */

namespace App\Components;

use App\Model\GeneModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;

use Nette\Database\Table\Selection;

class VariantGrid
{
    /** @var GeneModel @inject */
    public $geneModel;

    /** @var VariantModel @inject */
    public $variantModel;

    public function __construct(GeneModel $geneModel, VariantModel $variantModel)
    {
        $this->geneModel = $geneModel;
        $this->variantModel = $variantModel;
    }

    /**
     * @param null $panelId
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function create($panelId = null)
    {
        $geneModel = $this->geneModel;
        $grid = new DataGrid(null, 'variantDataGrid');

        $grid->setPrimaryKey(VariantModel::COL_ID);

        $grid->setDataSource($this->variantModel->listVariants($panelId));

        // Columns
        $grid->addColumnText(VariantModel::COL_ID, 'ID')
            ->setDefaultHide();

        $grid->addColumnLink(VariantModel::COL_GEN_ID, 'Gen', 'Gene:edit', VariantModel::COL_GEN_ID, ['id' => VariantModel::COL_GEN_ID])
            ->setRenderer(function ($item) use ($geneModel) {
                $gene = $geneModel->getGeneById($item[VariantModel::COL_GEN_ID])->fetch();

                return $gene[GeneModel::COL_NAME];
            })
            ->setSortable();

        $grid->addColumnText(VariantModel::COL_HGVS, 'HGVS');
        $grid->addColumnText(VariantModel::COL_RS, 'RS');
        $grid->addColumnDateTime(VariantModel::COL_CLINVAR_DATE, 'Clinvar datum')
            ->setDefaultHide();
        $grid->addColumnText(VariantModel::COL_CLINVAR_PROTEIN, 'Clinvar protein')
            ->setDefaultHide();
        $grid->addColumnText(VariantModel::COL_CLINVAR_VERDICT, 'Clinvar význam');
        $grid->addColumnText(VariantModel::COL_VERDICT, 'Význam')
            ->setDefaultHide();
        $grid->addColumnText(VariantModel::COL_NOTE, 'Poznámka')
            ->setDefaultHide();

        $grid->addColumnDateTime(VariantModel::COL_DATE_FROM, 'Datum od');
        $grid->addColumnDateTime(VariantModel::COL_DATE_TO, 'Datum do');

        // Filters
        $grid->addFilterText(VariantModel::COL_GEN_ID, 'Gen')
            ->setCondition(function ($selection, $value) use ($geneModel) {

                $genes = $geneModel->getGeneLikeName($value)->fetchAll();
                $geneIds = [];
                foreach ($genes as $gene) {
                    $geneIds[] = $gene[GeneModel::COL_ID];
                }

                if (count($geneIds) == 0) {
                    /** @var Selection $selection */
                    $selection->where(VariantModel::COL_GEN_ID, null);
                } else {
                    /** @var Selection $selection */
                    $selection->where(VariantModel::COL_GEN_ID, $geneIds);
                }

            });

        $grid->addFilterText(VariantModel::COL_HGVS, 'HGVS');
        $grid->addFilterText(VariantModel::COL_RS, 'RS');
        $grid->addFilterText(VariantModel::COL_CLINVAR_VERDICT, 'Clinvar význam');
        $grid->addFilterDateRange(VariantModel::COL_DATE_FROM, 'Datum od')
            ->setFormat('j. n. Y', 'd.m.yyyy');
        $grid->addFilterDateRange(VariantModel::COL_DATE_TO, 'Datum do')
            ->setFormat('j. n. Y', 'd.m.yyyy');

        return $grid;
    }
}