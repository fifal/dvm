<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 16:31
 */

namespace App\Model;

use Nette;

/**
 * Class EmployeeModel
 *
 * Table name: vysetrujici
 * Table structure:
 *      vysetrujici_id => PK, int
 *      vysetrujici_login => varchar(50)
 *      vysetrujici_password => varchar(MAX)
 *      vysetrujici_jmeno => varchar(50)
 *      vysetrujici_prijmeni => varchar(50)
 *      vysetrujici_titul_pred => varchar(50)
 *      vysetrujici_titul_za => varchar(50)
 *      vysetrujici_role => varchar(50)
 *
 * @package App\Model
 */
class EmployeeModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'vysetrujici';

    const COL_ID = 'vysetrujici_id';
    const COL_LOGIN = 'vysetrujici_login';
    const COL_PASSWORD = 'vysetrujici_password';
    const COL_FIRST_NAME = 'vysetrujici_jmeno';
    const COL_LAST_NAME = 'vysetrujici_prijmeni';
    const COL_DEGREE_BEFORE = 'vysetrujici_titul_pred';
    const COL_DEGREE_AFTER = 'vysetrujici_titul_za';
    const COL_ROLE = 'vysetrujici_role';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns employee (vysetrujici) by first name (vysetrujici_jmeno)
     *
     * @param $first_name : vysetrujici_jmeno
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeeByFirstName($first_name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_FIRST_NAME, $first_name);
    }

    /**
     * Returns employee (vysetrujici) by last name (vysetrujici_prijmeni)
     *
     * @param $last_name : vysetrujici_prijmeni
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeeByLastName($last_name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_LAST_NAME, $last_name);
    }

    /**
     * Returns employee (vysetrujici) by login (vysetrujici_login)
     *
     * @param $login : vysetrujici_login
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeeByLogin($login)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_LOGIN, $login);
    }

    /**
     * Return all employees sorted by last name in ascending order
     *
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeesOrderByLastNameASC()
    {
        return $this->database->table(self::TABLE_NAME)
            ->order(self::COL_LAST_NAME . ' ASC');
    }

    /**
     * Returns employee (vysetrujici) by id (vysetrujici_id)
     *
     * @param $id : vysetrujici_id
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeeById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Updates employee (vysetrujici) by id (vysetrujici_id)
     *
     * @param $id : vysetrujici_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updateEmployeeById($id, $data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Inserts into employee (vysetrujici)
     *
     * @param $data the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertEmployee($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from employee (vysetrujici) by id (vysetrujici_id)
     *
     * @param $id : vysetrujici_id
     * @return int
     */
    public function deleteEmployeeById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Updates employee's (vysetrujici) password (vysetrujici_password) by id (vysetrujici_id)
     *
     * @param $id : vysetrujici_id
     * @param $password : vysetrujici_password
     * @return int
     */
    public function updateEmployeePasswordById($id, $password)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update([self::COL_PASSWORD => $password]);
    }

    /**
     * Lists all employees from database
     *
     * @return Nette\Database\Table\Selection
     */
    public function listEmployees()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Get employees by last name ASC ordered, limited and with offset
     *
     * @param string $firstLetter
     * @param null $limit
     * @param null $offset
     * @return Nette\Database\Table\Selection
     */
    public function getEmployeesByLastName($firstLetter = '*', $limit = null, $offset = null)
    {
        $result = $this->listEmployees();

        if ($firstLetter == '*') {
            $result->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME);
        } else {
            $result->where(self::COL_LAST_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME);
        }

        if ($limit) {
            $result->limit($limit, $offset);
        }

        return $result;
    }
}