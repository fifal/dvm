<?php

namespace App\Model;

use Nette;

/**
 * Class PanelModel
 *
 * Table name: panel
 * Table structure:
 *      panel_id => PK, int
 *      panel_nazev => varchar
 *      panel_poznamka => varchar
 *
 * @package App\Model
 */
class PanelModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'panel';
  
    const COL_ID = 'panel_id';
    const COL_NAME = 'panel_nazev';
    const COL_NOTE = 'panel_poznamka';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Inserts into panel
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertPanel($data){
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from panel
     *
     * @param $id : panel_id
     */
    public function deletePanelById($id)
    {
        $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Returns number of panels
     *
     * @return int
     */
    public function getNumberOfPanels()
    {
        return $this->database->table(self::TABLE_NAME)->count("*");
    }

    /**
     * Returns all panels sorted by name (panel_nazev) in ascending order
     *
     * @param null $limit
     * @param null $offset
     * @return Nette\Database\Table\Selection
     */
    public function getPanelsOrderByNameASC($limit = null, $offset  = null)
    {
        if($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->order(self::COL_NAME . ' ASC')
                ->limit($limit, $offset);
        }
        else {
            return $this->database->table(self::TABLE_NAME)
                ->order(self::COL_NAME . ' ASC');
        }
    }

    /**
     * Returns panels by first letter of name (panel_nazev)
     *
     * @param $firstLetter, $limit, $offset
     * @return Nette\Database\Table\Selection
     */
    public function getPanelsByFirstLetter($firstLetter = '*', $limit = null, $offset = null)
    {
        if($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COL_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_NAME . ' ASC')
                ->limit($limit, $offset);
        }
        else {
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COL_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_NAME . ' ASC');
        }
    }

    /**
     * Returns panel by name (panel_nazev)
     *
     * @param $panel_name: panel_nazev
     * @return Nette\Database\Table\Selection
     */
    public function getPanelByName($panel_name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $panel_name);
    }

    /**
     * Returns panel by id (panel_id)
     *
     * @param $panel_id: panel_id
     * @return Nette\Database\Table\Selection
     */
    public function getPanelById($panel_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $panel_id);
    }

    /**
     * Updates panel by id (panel_id)
     *
     * @param $id : panel_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updatePanelById($id, $data){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Returns list of panels
     *
     * @return Nette\Database\Table\Selection
     */
    public function getListOfPanels()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Returns panel with name LIKE
     *
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getPanelLikeName($name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME . ' LIKE', '%' . $name . '%');
    }

}