<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 15:41
 */

namespace App\Model;

use Nette;

/**
 * Class PatientModel
 *
 * Table name: pacient
 * Table structure:
 *      pacient_id => PK, int
 *      pacient_jmeno => text
 *      pacient_prijmeni => text
 *      pacient_rc => varchar(10)
 *
 * @package App\Model
 */
class PatientModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'pacient';

    const COL_ID = 'pacient_id';
    const COL_FIRST_NAME = 'pacient_jmeno';
    const COL_LAST_NAME = 'pacient_prijmeni';
    const COL_PIN = 'pacient_rc';
  
    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns patient (pacient) by id (pacient_id)
     *
     * @param $id : pacient_id
     * @return Nette\Database\Table\Selection
     */
    public function getPatientById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns patient (pacient) by personal identification number (pacient_rc)
     *
     * @param $rc : pacient_id
     * @return Nette\Database\Table\Selection
     */
    public function getPatientByPerIdenNum($rc)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $rc);
    }

    /**
     * Returns patients by first name (pacient_jmeno)
     *
     * @param $first_name : pacient_jmeno
     * @return Nette\Database\Table\Selection
     */
    public function getPatientsByFirstName($first_name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_FIRST_NAME, $first_name);
    }

    /**
     * Returns patients by last name (pacient_prijmeni)
     *
     * @param $last_name : pacient_prijmeni
     * @return Nette\Database\Table\Selection
     */
    public function getPatientsByLastName($last_name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_LAST_NAME, $last_name);
    }

    /**
     * Returns patients by first letter of last name (pacient_prijmeni)
     *
     * @param $firstLetter , $limit, $offset
     * @return Nette\Database\Table\Selection
     */
    public function getPatientsByFirstLetter($firstLetter = '*', $limit = null, $offset = null)
    {
        if ($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COL_LAST_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME)
                ->limit($limit, $offset);
        } else {
            return $this->database->table('pacient')
                ->where(self::COL_LAST_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME);
        }
    }

    /**
     * Returns all patients sorted by last name in ascending order
     *
     * @return Nette\Database\Table\Selection
     */
    //TODO: seřazuje špatně diakritiku
    public function getPatientsOrderByLastNameASC($limit = null, $offset = null)
    {
        if ($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME)
                ->limit($limit, $offset);
        } else {
            return $this->database->table('pacient')
                ->order(self::COL_LAST_NAME . ' ASC, ' . self::COL_FIRST_NAME);
        }
    }

    /**
     * Inserts into patient (pacient)
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow : id of new patient
     */
    public function insertPatient($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * @param $id : pacient_id
     * @param $data : the structure of the table is in the class description
     */
    public function updatePatientById($id, $data)
    {
        $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Deletes from patient (pacient) by id (pacient_id)
     *
     * @param $patient_id : pacient_id
     */
    public function deletePatientById($patient_id)
    {
        $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $patient_id)
            ->delete();
    }

    /**
     * @return int
     */
    public function patientCount(){
        return $this->database->table(self::TABLE_NAME)
            ->count();
    }

}