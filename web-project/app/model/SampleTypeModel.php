<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 15:16
 */

namespace App\Model;

use Nette;

/**
 * Class SampleTypeModel
 *
 * Table name: typ_vzorku
 * Table structure:
 *      typ_vzorku_id => PK, int
 *      typ_vzorku_nazev => text
 *
 * @package App\Model
 */
class SampleTypeModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'typ_vzorku';

    const COL_ID = 'typ_vzorku_id';
    const COL_NAME = 'typ_vzorku_nazev';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns sample type (typ_vzorku) by sample type id (typ_vzorku_id)
     *
     * @param $id : typ_vzorku_id
     * @return Nette\Database\Table\Selection
     */
    public function getSampleTypeById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns sample type (typ_vzorku) by sample type name(typ_vzorku_nazev)
     *
     * @param $name : typ_vzorku_nazev
     * @return Nette\Database\Table\Selection
     */
    public function getSampleTypeByName($name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $name);
    }

    /**
     * @return array|Nette\Database\Table\IRow[]|Nette\Database\Table\Selection
     */
    public function getSamples()
    {
        return $this->database->table(self::TABLE_NAME)->fetchAll();
    }

    /**
     * Inserts into SampleType (typ_vzorku)
     *
     * @param $data : the structure of the table is in the class description
     */
    public function insertSampleType($data)
    {
        $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Updates sample type (typ_vzorku) by id (typ_vzorku_id)
     *
     * @param $id : typ_vzorku_id
     * @param $data : the structure of the table is in the class description
     */
    public function updateSampleTypeById($id, $data)
    {
        $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Deletes from sample type (typ_vzorku) by id (typ_vzorku_id)
     *
     * @param $id : typ_vzorku_id
     */
    public function deleteSampleTypeById($id)
    {
        $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Returns sample types with name LIKE
     *
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getSampleTypeLikeName($name){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME . ' LIKE', '%' . $name . '%');
    }
}