<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 17:37
 */

namespace App\Model;

use Nette;

/**
 * Class VariantExaminationModel
 *
 * Table name: varianta_vysetreni
 * Table structure:
 *      varianta_vysetreni_varianta_id => PK, FK, int
 *      varianta_vysetreni_vysetreni_id => PK, FK, int
 *      varianta_vysetreni_dna_gen_id => FK, int
 *
 * @package App\Model
 */
class VariantExamModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'varianta_vysetreni';

    const COL_VARIANT_ID = 'varianta_vysetreni_varianta_id';
    const COL_EXAMINATION_ID = 'varianta_vysetreni_vysetreni_id';
    const COL_DNA_GEN_ID = 'varianta_vysetreni_dna_gen_id';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns variant examination (varianta_vysetreni) by examination id (varianta_vysetreni_vysetreni_id)
     *
     * @param $exam_id : varianta_vysetreni_vysetreni_id
     * @return Nette\Database\Table\Selection
     */
    public function getVarExamByExamId($exam_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_EXAMINATION_ID, $exam_id);
    }

    /**
     * Inserts into variant examination (varianta_vysetreni)
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertVariantExamination($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from variant examination (varianta_vysetreni)
     *
     * @param $variant_id : varianta_vysetreni_varianta_id
     * @param $examination_id : varianta_vysetreni_vysetreni_id
     * @return int
     */
    public function deleteVarExamByPK($variant_id, $examination_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_VARIANT_ID, $variant_id)
            ->where(self::COL_EXAMINATION_ID, $examination_id)
            ->delete();
    }

    /**
     * Returns variantexam by variant id
     *
     * @param $varId
     * @return Nette\Database\Table\Selection
     */
    public function getVarExamByVarId($varId)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_VARIANT_ID, $varId);
    }

}