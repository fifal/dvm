<?php

namespace App\Model\Cron;

use App\Model\GeneModel;
use App\Model\VariantModel;
use App\Utils\Varsome\VarsomeAPI;
use App\Utils\Varsome\VarsomeVariant;
use Exception;
use Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

/**
 * Command pro aktualizaci Clinvar sloupců v tabulce pomocí Varsome Api
 *      - Pouští z příkazové řádky web-project/www/ : "php index.php Cron:updateVariantsVarsome"
 *
 * Class UpdateVariantsFromVarsome
 * @package App\Model\Cron
 */
class UpdateVariantsFromVarsome extends Command
{
    /** @var VariantModel @inject */
    public $variantModel;

    /** @var GeneModel @inject */
    public $geneModel;

    /** @var VarsomeAPI @inject */
    public $varsomeAPI;


    /** @var string Command name */
    const NAME = "Cron:updateVariantsVarsome";

    public function __construct(VariantModel $variantModel, GeneModel $geneModel, VarsomeAPI $varsomeAPI)
    {
        parent::__construct();
        $this->variantModel = $variantModel;
        $this->geneModel = $geneModel;
        $this->varsomeAPI = $varsomeAPI;
    }

    /**
     * Sets command name and description
     */
    protected function configure()
    {
        $this->setName(self::NAME)
            ->setDescription('Aktualizuje Clinvar sloupce varianty pomocí Varsome API');
    }

    /**
     * Executes command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try
        {
            $variants = $this->variantModel->listVariants()->where(VariantModel::COL_CLINVAR_VERDICT, null);
            $output->writeln('Pocet zaznamu k aktualizaci: ' . count($variants));

            $updated = 0;
            foreach ($variants as $variant)
            {
                if ($this->updateVariant($variant))
                {
                    $updated++;
                }
            }

            $output->writeln('Uspech! Pocet aktualizovanych variant: ' . $updated);
            \Tracy\Debugger::log(self::NAME . ' - uspech! Pocet aktualizovanych variant: ' . $updated, 'cron.success');
        } catch (Exception $ex)
        {
            $output->writeln('Nepodarilo se aktualizovat varianty: ' . $ex->getMessage());
            \Tracy\Debugger::log(self::NAME . ' - neuspech: ' . $ex->getMessage(), 'cron.error');
        }
    }

    /**
     * Updates variant's Clinvar columns from Varsome API
     *
     * @param $variant : Selection
     * @return bool
     */
    private function updateVariant($variant)
    {
        if (!isset($variant[VariantModel::COL_HGVS]) && !isset($variant[VariantModel::COL_RS]))
        {
            return false;
        }

        if (!empty($variant[VariantModel::COL_RS]))
        {
            $rs = $variant[VariantModel::COL_RS];
            // If input doesn't contain rs at start add it
            if (strpos($rs, 'rs') === false)
            {
                $rs = 'rs' . $rs;
            }

            $result = $this->varsomeAPI->getVariantLookup($rs);
        } elseif (!empty($variant[VariantModel::COL_HGVS]))
        {
            $var = str_replace(' ', '', $variant[VariantModel::COL_HGVS]);
            $result = $this->varsomeAPI->getVariantLookup($var);
        }

        $bestStars = -1;
        $bestVariant = null;

        /** @var VarsomeVariant $item */
        /** @var VarsomeVariant $bestVariant */
        foreach ($result as $item)
        {
            if ($item->getClinvarVerdictStars() > $bestStars)
            {
                $bestStars = $item->getClinvarVerdictStars();
                $bestVariant = $item;
            }
        }

        if($bestVariant == null)
        {
            return false;
        }

        $variant = $this->variantModel->getVariantById($variant[VariantModel::COL_ID]);

        $data =
            [
                VariantModel::COL_CLINVAR_VERDICT => $bestVariant->getClinvarVerdict(),
                VariantModel::COL_CLINVAR_DATE => $bestVariant->getLastEvalDate()
            ];

        if ($variant->update($data))
        {
            return true;
        }

        return false;
    }
}