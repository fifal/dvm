<?php

namespace App\Model\Cron;

use App\Model\GeneModel;
use App\Model\VariantModel;
use Exception;
use Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

/**
 * Command pro aktualizaci genu dané varianty, protože v původních souborech takové info není
 *      - Pouští z příkazové řádky web-project/www/ : "php index.php Cron:updateVariantsGene"
 *
 * Class UpdateVariantsGeneCommand
 * @package App\Model\Cron
 */
class UpdateVariantsGeneCommand extends Command
{
    /** @var VariantModel @inject */
    public $variantModel;

    /** @var GeneModel @inject */
    public $geneModel;


    /** @var string Command name */
    const NAME = "Cron:updateVariantsGene";

    public function __construct(VariantModel $variantModel, GeneModel $geneModel)
    {
        parent::__construct();
        $this->variantModel = $variantModel;
        $this->geneModel = $geneModel;
    }

    /**
     * Sets command name and description
     */
    protected function configure()
    {
        $this->setName(self::NAME)
            ->setDescription('Aktualizuje ke kteremu varianta patri podle prefixu pred dvojteckou v HGVS');
    }

    /**
     * Executes command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try
        {
            $variants = $this->variantModel->listVariants()->where(VariantModel::COL_GEN_ID, null);
            $output->writeln('Pocet zaznamu k aktualizaci: ' . count($variants));

            $updated = 0;
            foreach ($variants as $variant)
            {
                if ($this->updateVariant($variant))
                {
                    $updated++;
                }
            }

            $output->writeln('Uspech! Pocet aktualizovanych variant: ' . $updated);
            \Tracy\Debugger::log(self::NAME . ' - uspech! Pocet aktualizovanych variant: ' . $updated, 'cron.success');
        } catch (Exception $ex)
        {
            $output->writeln('Nepodarilo se aktualizovat varianty: ' . $ex->getMessage());
            \Tracy\Debugger::log(self::NAME . ' - neuspech: ' . $ex->getMessage(), 'cron.error');
        }
    }

    /**
     * Updates variant from Varsome API
     *  - input example: BRCA2: c.-27A>G
     *
     * @param $variant : Selection
     * @return bool
     */
    private function updateVariant($variant)
    {
        if (!isset($variant[VariantModel::COL_HGVS]))
        {
            return false;
        }

        $hgvs = $variant[VariantModel::COL_HGVS];

        // Find : in variant
        $colonPos = strpos($hgvs, ':');
        if (!$colonPos)
        {
            return false;
        }

        // Get gene name
        $geneName = substr($hgvs, 0, $colonPos);
        if (!$geneName)
        {
            return false;
        }

        // Find ID of gene with given name in database
        $gene = $this->geneModel->getGeneByName($geneName)->fetch();
        if (!isset($gene) && !isset($gene[GeneModel::COL_ID]))
        {
            return false;
        }

        $geneId = $gene[GeneModel::COL_ID];

        if ($this->variantModel->updateVariantById($variant[VariantModel::COL_ID], [VariantModel::COL_GEN_ID => $geneId]))
        {
            return true;
        }

        return false;
    }
}