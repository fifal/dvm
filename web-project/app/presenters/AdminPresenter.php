<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 24.5.18
 * Time: 13:28
 *
 */

namespace App\Presenters;

use App\Components\BootstrapForm;
use App\Components\UserEditForm;
use App\Components\UserPasswordForm;
use App\Model\AdminResetModel;
use App\Model\Auth\AuthenticatorReset;

;

use App\Model\DnaModel;
use App\Model\EmployeeModel;
use App\Model\ExaminationModel;
use App\Model\GeneModel;
use App\Model\PatientModel;
use App\Model\RoleModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;
use App\Utils\PasswordValidator;
use DVM\DbImport;
use DVM\DbImportTableEnums;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

class AdminPresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context */
    private $context;

    // MODELS
    /** @var EmployeeModel @inject */
    public $employeeModel;

    /** @var RoleModel @inject */
    public $roleModel;

    /** @var AdminResetModel @inject */
    public $adminResetModel;

    /** @var DnaModel @inject */
    public $dnaModel;

    /** @var GeneModel @inject */
    public $geneModel;

    /** @var VariantModel @inject */
    public $variantModel;

    /** @var ExaminationModel @inject */
    public $examinationModel;

    /** @var PatientModel @inject */
    public $patientModel;

    // COMPONENTS
    /** @var UserEditForm @inject */
    public $userFormFactory;

    /** @var UserPasswordForm @inject */
    public $userPasswordFormFactory;

    /** @var \FilesStorage @inject */
    public $fileStorage;

    // DB IMPORT THINGS
    private $tables;
    private $columns;

    /** @var DbImport @inject */
    public $dbImport;

    public function __construct(Nette\Database\Context $context)
    {
        $this->context = $context;
    }

    public function startup()
    {
        parent::startup();
        if (!$this->user->isAllowed('admin')
            && $this->getAction() != 'resetAdminPassword'
            && $this->getAction() != 'generateAdminPassword') {
            throw new Nette\Application\ForbiddenRequestException();
        }
    }

    public function renderDefault(){
        $this->template->patientCount = $this->patientModel->patientCount();
        $this->template->geneCount = $this->geneModel->getNumberOfGenes();

        //TODO: po mergnutí model-refactor změnit na TABLE_NAME
        $this->template->variantCount = $this->variantModel->database->table('varianta')->count();
        $this->template->dnaCount = $this->dnaModel->database->table('dna')->count();
        $this->template->examinationCount = $this->examinationModel->database->table('vysetreni')->count();
    }

    //
    //
    // DATABASE FILE UPLOAD AND IMPORT
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initialization of dbImport, tables and columns
     *      has to be action, cause it's executed before createComponentDbImportForm
     *
     * @param $file : database file name
     * @throws Nette\Application\AbortException
     */
    public function actionDbImport($file)
    {
        if(!isset($file)){
            $this->flashMessage('Chyba při zpracování souboru nebo nebyl vybrán žádný soubor.', 'error');
            $this->redirect('Admin:db-upload');
        }

        $filePath = $this->fileStorage->getPath() . $file;
        if (!file_exists($filePath)) {
            $this->flashMessage('Chyba při zpracování souboru ' . $file, 'error');
        }

        $this->dbImport->setFileConnection($filePath);
        $this->tables = $this->dbImport->getTableNames();
        $this->columns = $this->dbImport->getColumnNames();
    }

    /**
     * Render for dbImport
     *
     * @param $file : database file name
     */
    public function renderDbImport($file)
    {
        $filePath = $this->fileStorage->getPath() . $file;
        if (!file_exists($filePath)) {
            $this->flashMessage('Chyba při zpracování souboru ' . $file, 'error');
        }

        $this->template->tables = $this->tables;
        $this->template->columns = $this->columns;
    }

    /**
     * Creates database upload form, with one upload input and submit
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentDbUploadForm()
    {
        $form = new BootstrapForm;

        $form->addUpload('file', 'Výběr databázového souboru')
            //->setRequired('Nebyl vybrán soubor s databází')
            ->setHtmlAttribute('class', 'form-control mb-2');

        $form->addSubmit('send', 'Importovat databázi')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'uploadDbSuccess'];

        return $form;
    }

    /**
     * Handles database upload form success function
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function uploadDbSuccess(Nette\Application\UI\Form $form, $values)
    {
        if (isset($values->file) && $values->file->getName() !== NULL) {
            $this->fileStorage->moveFile($values->file);
            $this->redirect('Admin:dbImport', array('file' => $values->file->getName()));
        } else {
            $this->redirect('Admin:dbupload');
        }
    }

    /**
     * Creates Database import form
     *
     * $types = array with all possible tables in new database
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentDbImportForm()
    {
        $form = new BootstrapForm;

        $types = [
            DbImportTableEnums::DONT_IMPORT => 'NEIMPORTOVAT!',
            DbImportTableEnums::DNA => 'Dna',
            DbImportTableEnums::PATIENT => 'Pacient',
            DbImportTableEnums::SAMPLE_TYPE => 'Typ vzorku',
            DbImportTableEnums::VARIANT => 'Varianta',
            DbImportTableEnums::EMPLOYEE => 'Vyšetřující'
        ];

        // For each table create select with possible table types from $tables array
        if (isset($this->tables)) {
            foreach ($this->tables as $table) {
                $form->addSelect($table, 'Tabulka ' . $table . ': ', $types)
                    ->setHtmlAttribute('class', 'form-control mb-2');
            }
        }

        $form->addSubmit('send', 'Importovat databázi')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'importFormSuccess'];

        return $form;
    }

    /**
     * Handles database import form success function
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     */
    //TODO: ošetření a tak
    public function importFormSuccess(Nette\Application\UI\Form $form, $values)
    {
        foreach ($this->tables as $table) {
            if (DbImportTableEnums::getEnum($values[$table]) != DbImportTableEnums::DONT_IMPORT) {
                $this->dbImport->importType($table, DbImportTableEnums::getEnum($values[$table]));
                $this->flashMessage('Tabulka ' . $table . ' byla importována.', 'success');
            }
        }
    }


    //
    //
    // USER LIST
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentUsersDataGrid()
    {
        $roles = $this->roleModel->getRoles()->fetchPairs(RoleModel::COL_ID, RoleModel::COL_NAME);
        $roles = [null => 'nepřiřazeno'] + $roles;

        $grid = new DataGrid();

        $grid->setPrimaryKey(EmployeeModel::COL_ID);

        $grid->setDataSource($this->employeeModel->listEmployees());
        $grid->setColumnsHideable();

        // Columns
        $grid->addColumnNumber(EmployeeModel::COL_ID, 'ID')->setDefaultHide();
        $grid->addColumnText(EmployeeModel::COL_LAST_NAME, 'Příjmení');
        $grid->addColumnText(EmployeeModel::COL_FIRST_NAME, 'Jméno');
        $grid->addColumnText(EmployeeModel::COL_LOGIN, 'Login');
        $grid->addColumnText(EmployeeModel::COL_ROLE, 'Role')
            ->setReplacement($roles);

        // Filters
        $grid->addFilterText(EmployeeModel::COL_LAST_NAME, 'Příjmení');
        $grid->addFilterText(EmployeeModel::COL_FIRST_NAME, 'Jméno');
        $grid->addFilterText(EmployeeModel::COL_LOGIN, 'Login');
        $grid->addFilterSelect(EmployeeModel::COL_ROLE, 'Role', $roles);

        // Actions
        $grid->addAction('edit', null, 'Admin:edit-user', ['id' => EmployeeModel::COL_ID])
            ->setTitle('Editovat vyšetřujícího')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('delete', null, 'deleteUser!', ['id' => EmployeeModel::COL_ID])
            ->setConfirm('Opravdu chcete uživatele %s odstranit?', EmployeeModel::COL_LAST_NAME)
            ->setTitle('Odstranit')
            ->setIcon('trash')
            ->setClass('danger ajax');

        return $grid;
    }

    /**
     * Deletes user from database
     *
     * @param $id : of user to delete
     * @throws Nette\Application\AbortException
     */
    public function handleDeleteUser($id){
        $roles = $this->roleModel->getRoles()->fetchPairs(RoleModel::COL_NAME, RoleModel::COL_ID);
        $user = $this->employeeModel->getEmployeeById($id)->fetch();

        $userRole = -1;
        // If user was found and his role has been set
        if (isset($user) && isset($user->vysetrujici_role))
        {
            $userRole = $user->vysetrujici_role;
        }

        // If userRole is set and current user for delete is Admin
        if ($userRole != -1 && $userRole == $roles['admin'])
        {
            $adminCount = $this->employeeModel->listEmployees()->where(EmployeeModel::COL_ROLE, $roles['admin'])->count();

            // Check if there is atleast one more administrator in database before deleting current user
            if ($adminCount <= 1)
            {
                $this->flashMessage('Nelze smazat posledního administrátora z databáze.', 'danger');
                return;
            }
        }

        $result = $this->employeeModel->deleteEmployeeById($id);

        if ($result > 0)
        {
            $this->flashMessage('Uživatel byl úspěšně smazán', 'success');
        } else
        {
            $this->flashMessage('Nastala chyba při mazání uživatele', 'error');
        }

        if ($id == $this->getUser()->getId())
        {
            $this->getUser()->logout();

            $this->flashMessage('Váš uživatelský účet byl smazán a proto byl odhlášen.', 'danger');
            $this->redirect('Homepage:default');
        }

        $this->redrawControl('flashes');
        $this['usersDataGrid']->reload();
    }

    //
    //
    // USER EDIT
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets user edit alert message
     *
     * @param $message : string
     * @param $type : string, from bootstrap (success, danger, info, ...)
     */
    public function setUserEditAlert($message, $type)
    {
        $this->template->userInfo = $message;
        $this->template->userType = $type;
    }

    /**
     * Sets user password edit alert message
     *
     * @param $message
     * @param $type : string, from bootstrap (success, danger, info, ...)
     */
    public function setUserEditPasswordAlert($message, $type)
    {
        $this->template->passwordInfo = $message;
        $this->template->passwordType = $type;
    }

    /**
     * Creates new form for editing user information
     *
     * @return mixed
     */
    protected function createComponentEditUserForm()
    {
        $form = $this->userFormFactory->create();

        $form->addHidden('vysetrujici_id', $this->getParameter('id'));
        $data = $this->employeeModel->getEmployeeById($this->getParameter('id'))->fetch();
        $form->setDefaults($data);

        $form->addSubmit('send', 'Upravit uživatele')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'editUserSuccess'];

        return $form;
    }

    /**
     * Edits user details
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function editUserSuccess($form, $values)
    {
        // Employee ID
        $id = $values->vysetrujici_id;

        // Get employee
        $employeeRow = $this->employeeModel->getEmployeeById($id)->fetch();

        // New username value
        $userNewLogin = $values->vysetrujici_login;

        // If username was changed -> check if it is available
        if ($employeeRow->vysetrujici_login != $userNewLogin) {
            // Get user by login name
            $userExists = $this->employeeModel->getEmployeeByLogin($userNewLogin)->count();

            // If there is more than 0 results -> user exists -> show error message and return
            if ($userExists) {
                $this->setUserEditAlert('Uživatel s daným přihlašovacím jménem již existuje. Vyberte prosím jiné.', 'danger');
                return;
            }
        }

        // Get roles and their IDs
        $roles = $this->roleModel->getRoles()->fetchPairs('role_nazev', 'role_id');

        // Checks if there's at least one more user with admin role when changing user role to 'user'
        if ($employeeRow->ref('role') != null && $employeeRow->ref('role')->role_nazev == 'admin' && $values->vysetrujici_role == $roles['user']) {
            $adminCount = $this->employeeModel->listEmployees()->where('vysetrujici_role', $roles['admin'])->count();

            // No other admin is at database -> redirect and show error message
            if ($adminCount <= 1) {
                $this->setUserEditAlert('Nelze odebrat administrátorskou roli. V aplikaci by nezbyl žádný administrátor.
                    Přiřaďte roli administrátora alespoň jednomu dalšímu uživateli.', 'danger');
                return;
            }
        }

        // Get user role for later check if role has changed so we can update users identity
        $oldUserRole = $this->employeeModel->getEmployeeById($values->vysetrujici_id)->fetch()->vysetrujici_role;

        // Unset user ID in values
        unset($values['vysetrujici_id']);

        // Update user profile
        $this->setUserEditAlert('Uživatelské údaje byly uloženy.', 'success');

        $result = $this->employeeModel->updateEmployeeById($id, $values);

        // Check for errors
        if ($result == 0) {
            $this->setUserEditAlert('Nastala chyba při upravování uživatelského profilu. Zkuste prosím formulář odeslat znovu.', 'danger');
            return;
        }

        // If editing currently logged in user -> update his identity
        if ($id == $this->getUser()->getId()) {
            $this->updateUserIdentity($values, $oldUserRole);
        }
    }

    /**
     * Updates user identity
     *
     * @param $values : values from edit form
     * @throws Nette\Application\AbortException
     */
    public function updateUserIdentity($values, $oldUserRole)
    {
        $currentIdentity = $this->getUser()->getIdentity();

        foreach ($values as $key => $value) {
            $currentIdentity->$key = $value;
        }

        if ($oldUserRole != $values->vysetrujici_role) {
            $this->flashMessage('Byla změněna role přihlášeného uživatele. Je nutné se znova přihlásit.', 'danger');
            $this->getUser()->logout();
            $this->redirect('User:login');
        }
    }

    /**
     * Shows edit user page
     *
     * @param $id
     */
    public function renderEditUser($id)
    {
        $employee = $this->employeeModel->getEmployeeById($id)->fetch();
        $this->template->employee = $employee->vysetrujici_jmeno . ' ' . $employee->vysetrujici_prijmeni;
    }

    /**
     * Creates form for password editing
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentEditUserPasswordForm()
    {
        $form = $this->userPasswordFormFactory->create();

        $form->addHidden('vysetrujici_id', $this->getParameter('id'));

        $form->addSubmit('sendEditPassword', 'Změnit heslo')
            ->setHtmlAttribute('class', 'form-control btn btn-warning');

        $form->onSuccess[] = [$this, 'editUserPasswordSuccess'];

        return $form;
    }

    /**
     * Checks if submited form for edit password had proper values and changes user's password in Employee model
     *
     * @param $form
     * @param $values
     */
    public function editUserPasswordSuccess($form, $values)
    {
        $pass = $values->password;
        $passConfirm = $values->passwordConfirm;
        $id = $values->vysetrujici_id;

        // If passwords are NOT equal or password is NOT valid
        if ($pass !== $passConfirm || !PasswordValidator::isValidPassword($pass)) {
            $this->setUserEditPasswordAlert('Hesla se neshodují, nebo heslo nesplňuje požadavky.', 'danger');
            return;
        }

        // Create password hash and update password in database
        $passHash = Passwords::hash($pass);
        $result = $this->employeeModel->updateEmployeePasswordById($id, $passHash);

        // Check for errors, If result = 0 -> zero affected rows -> error
        if ($result == 0) {
            $this->setUserEditPasswordAlert('Nastala chyba při změně hesla. Zkuste prosím formulář odeslat znovu.', 'danger');
            return;
        }

        $this->setUserEditPasswordAlert('Heslo bylo úspěšně změněno', 'success');
    }

    //
    //
    // USER ADD
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates user add form
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentAddUserForm()
    {
        $form = $this->userFormFactory->create();

        $form->onSuccess[] = [$this, 'addUserSuccess'];

        $form->addSubmit('send', 'Přidat uživatele')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        return $form;
    }

    /**
     * Adds new user into database and redirects to edit user page
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function addUserSuccess($form, $values)
    {
        $userExists = $this->employeeModel->getEmployeeByLogin($values->vysetrujici_login)->count();

        // Check if user with given username exists
        if ($userExists) {
            $this->setUserEditAlert('Uživatel s daným přihlašovacím jménem již existuje. Vyberte prosím jiné.', 'danger');
            return;
        }

        $result = $this->employeeModel->insertEmployee($values);

        // Check for errors
        if (!$result) {
            $this->setUserEditAlert('Nastala chyba při přidávání uživatele. Zkuste prosím formulář odeslat znovu.', 'danger');
            return;
        }

        // Redirect to user edit form
        $this->flashMessage('Uživatel byl úspěšně přidán', 'success');
        $this->redirect('Admin:editUser', $result->getPrimary());
    }

    //
    //
    // ADMIN PASSWORD RESET
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates form with one input for token string
     *
     * @return Form: Form
     */
    protected function createComponentResetAdminForm()
    {
        $form = new BootstrapForm;

        $form->addText('token', 'Zadejte token:')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired('Pole token je povinné.');

        $form->addSubmit('send', 'Resetovat heslo')
            ->setHtmlAttribute('class', 'form-control btn btn-warning');

        $form->onSuccess[] = [$this, 'resetAdminSuccess'];

        return $form;
    }

    /**
     * Handles reset admin password form
     *
     * @param $form : Form
     * @param $values : values from form
     * @throws Nette\Security\AuthenticationException
     * @throws Nette\Application\AbortException
     */
    public function resetAdminSuccess($form, $values)
    {
        $tokenRow = $this->adminResetModel->getToken($values['token'])->fetch();

        // Correct token and wasn't used yet -> add user temp role "reset" and redirect to password reset
        if (isset($tokenRow->admin_reset_heslo) && !$tokenRow->admin_reset_pouzito) {
            $this->getUser()->setAuthenticator(new AuthenticatorReset($this->adminResetModel));
            $this->getUser()->login($values['token']);

            $this->redirect('Admin:generateAdminPassword');
        } else {
            $this->template->resetAdminAlert = "Token buď neexistuje nebo už byl jednou použit.";
            $this->template->resetAdminAction = "danger";
        }
    }

    /**
     * Renders generate admin password form
     *
     * @throws Nette\Application\ForbiddenRequestException
     */
    public function renderGenerateAdminPassword()
    {
        if (!$this->getUser()->isAllowed('generate-admin-password')) {
            throw new Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * Creates password reset form which contains two inputs for password and one select to choose
     *      admin account for password reset
     *
     * @return Form
     */
    protected function createComponentGenerateAdminPassword()
    {
        // Creates password form
        $form = $this->userPasswordFormFactory->create();

        // Gets all users with "admin" role
        $adminRoleId = $this->roleModel->getRoleByName('admin')->fetch()->role_id;
        $admins = $this->employeeModel->listEmployees()->where('vysetrujici_role', $adminRoleId)->fetchAll();

        // Add all admins into array for select
        $adminsItems = array();
        foreach ($admins as $admin) {
            $adminsItems[$admin->vysetrujici_id] = $admin->vysetrujici_login;
        }

        $form->addSelect('vysetrujici_id', 'Účet pro změnu hesla', $adminsItems)
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired(true);

        $form->addSubmit('sendEditPassword', 'Změnit heslo')
            ->setHtmlAttribute('class', 'form-control btn btn-warning');

        $form->onSuccess[] = [$this, 'generateAdminPasswordSuccess'];

        return $form;
    }

    /**
     * Handles generate admin password form
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function generateAdminPasswordSuccess($form, $values)
    {
        $password = $values->password;
        $passwordConfirm = $values->passwordConfirm;

        // Should never be true
        if ($password != $passwordConfirm) {
            $this->template->generateAdminAlert = "Hesla se neshodují!";
            $this->template->generateAdminAction = "danger";
            return;
        }

        // Update user password
        $this->employeeModel->updateEmployeePasswordById($values->vysetrujici_id, Passwords::hash($password));

        $this->adminResetModel->setTokenUsed($this->getUser()->getIdentity()->getData()['token']);

        // Logout user with reset role
        $this->getUser()->logout();

        // Redirect to login page
        $this->flashMessage('Heslo bylo úspěšně změněno, nyní se můžete přihlásit.', 'success');
        $this->redirect('User:login');
    }
}