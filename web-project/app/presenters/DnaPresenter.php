<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 3. 7. 2018
 * Time: 16:41
 */

namespace App\Presenters;

use App\Components\DnaEditForm;
use App\Model\DnaModel;
use App\Model\ExaminationModel;
use App\Model\PatientModel;
use Nette;
use App\Components\PatientEditForm;

class DnaPresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var PatientModel @inject
     */
    public $patientModel;

    /**
     * @var DnaModel @inject
     */
    public $dnaModel;

    /** @var PatientEditForm @inject */
    public $patientEditFormFactory;

    /** @var ExaminationModel @inject */
    public $examinationModel;

    /**
     * @var DnaEditForm @inject
     */
    public $dnaEditFormFactory;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentDnaNewForm()
    {
        $form = $this->patientEditFormFactory->create();

        $data = $this->patientModel->getPatientById($this->getParameter('id'))->fetch();
        if ($data) {
//            $form['pacient_jmeno']->setDisabled();
            $form['pacient_jmeno']->setAttribute('readonly', true);
            $form['pacient_prijmeni']->setAttribute('readonly', true);
            $form['pacient_rc']->setAttribute('readonly', true);
            $form->setDefaults($data);
        }


        $form = $this->dnaEditFormFactory->createControls($form);

        $form->addSubmit('send', 'Přidat vzorek DNA')
            ->setHtmlAttribute('class', 'form-control btn btn-success mt-3');

        $form->onSuccess[] = [$this, 'newDnaSuccess'];

        $form['dna_datum']->setDefaultValue(date('d.m.Y'));

        return $form;
    }

    public function newDnaSuccess($form, $values)
    {
        $values->dna_pacient_id = $this->getParameter('id');
        $patient = array(
            "pacient_jmeno" => $values->pacient_jmeno,
            "pacient_prijmeni" => $values->pacient_prijmeni,
            "pacient_rc" => $values->pacient_rc);
        unset($values->pacient_jmeno);
        unset($values->pacient_prijmeni);
        unset($values->pacient_rc);

        $values->dna_datum = date("Y-m-d", strtotime($values->dna_datum));

        if ($this->dnaModel->getDnaBySampleNumber($values->dna_cislo_vzorku) != null) {
            $this->flashMessage('Číslo tohoto vzorku už existuje.', 'danger');
        }
        else {
            if ($this->patientModel->getPatientById($values->dna_pacient_id)->fetch()) {
                $this->dnaModel->insertDna($values);
                $this->flashMessage('Vzorek DNA byl přidán.', 'success');
                $this->redirect('Dna:add', $values->dna_pacient_id);
            } else if (!$this->getParameter('id')) {
                $row = $this->patientModel->insertPatient($patient);
                $values->dna_pacient_id = $row->pacient_id;
                $this->dnaModel->insertDna($values);
                $this->flashMessage('Byl vytvořen nový pacient a přidán vzorek DNA.', 'success');
                $this->redirect('Dna:add', $values->dna_pacient_id);
            }
        }
    }

    public function renderAdd($id = null)
    {
        if ($id) {
            $this->template->id = $id;
        }
    }

    /**
     * Renders details about dna sample
     *
     * @param $id
     */
    public function renderDetail($id){
        $dna = $this->dnaModel->getDnaById($id)->fetch();
        $this->template->dna = $dna;
        $this->template->patient = $this->patientModel->getPatientById($dna->dna_pacient_id)->fetch();
    }

    /**
     * Creates dna sample detail form
     *
     * @return Nette\Application\UI\Form
     */
    public function createComponentDnaDetailForm()
    {
        $dna = $this->dnaModel->getDnaById($this->getParameter('id'))->fetch();
        $form = $this->dnaEditFormFactory->create();
        $form->addHidden('dna_id');
        $form->setDefaults($dna);
        $form['dna_datum']->setDefaultValue(date('d.m.Y', strtotime($dna->dna_datum)));

        $form->addSubmit('editDna', 'Upravit')
            ->setHtmlAttribute('class', 'btn btn-success float-right ml-1')
            ->onClick[] = [$this, 'editDnaSuccess'];

        $form->addSubmit('deleteDna', 'Smazat')
            ->setHtmlAttribute('class', 'btn btn-danger float-right')
            ->onClick[] = [$this, 'deleteDnaSuccess'];

        return $form;
    }

    /**
     * Edits dna sample
     *
     * @param $form
     * @param $values
     */
    public function editDnaSuccess($form, $values)
    {
        $dna = $this->dnaModel->getDnaById($values->dna_id)->fetch();
        unset($values->dna_id);
        $values->dna_pacient_id = $dna->dna_pacient_id;
        $values->dna_datum = date("Y-m-d", strtotime($values->dna_datum));
        $result = $this->dnaModel->updateDnaById($dna->dna_id,$values);

        if ($result) {
            $this->flashMessage('Vzorek DNA byl upraven.', 'success');
        } else {
            $this->flashMessage('Nastala chyba při úpravě DNA.', 'danger');
        }

    }

    /**
     * Deletes DNA sample
     *
     * @param $form
     * @param $values
     */
    public function deleteDnaSuccess($form, $values){

        $dna = $this->dnaModel->getDnaById($values->dna_id)->fetch();

        if($this->examinationModel->getExaminationByDnaID($values->dna_id)->count() != 0){
            $this->flashMessage('Nelze smazat vzorek DNA přiřazený k vyšetření.', 'danger');
            return;
        }

        $result = $this->dnaModel->deleteDnaById($values->dna_id);

        if ($result > 0) {
            $this->flashMessage('Vzorek byl úspěšně smazán.', 'success');
            $this->redirect('Patient:detail', $dna->dna_pacient_id);
        } else {
            $this->flashMessage('Nastala chyba při mazání vzorku.', 'danger');
        }

    }

}