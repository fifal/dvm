<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 02.07.2018
 * Time: 20:32
 */

namespace App\Presenters;

use App\Model\GeneModel;
use App\Components\GeneForm;
use App\Model\PanelGeneModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;
use App\Utils\Varsome\VarsomeAPI;
use Nette\Application\UI\Form;

use Nette;

class GenePresenter extends Nette\Application\UI\Presenter
{

    /** @var GeneModel */
    private $geneModel;

    /** @var  VariantModel */
    private $variantModel;

    /** @var PanelGeneModel */
    private $panelGeneModel;

    /** @var GeneForm */
    private $geneFormFactory;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * GenePresenter constructor.
     * @param GeneModel $geneModel
     * @param GeneForm $geneForm
     * @param VariantModel $variantModel
     * @param PanelGeneModel $panelGeneModel
     */
    public function __construct(GeneModel $geneModel, GeneForm $geneForm, VariantModel $variantModel, PanelGeneModel $panelGeneModel)
    {
        $this->geneModel = $geneModel;
        $this->geneFormFactory = $geneForm;
        $this->panelGeneModel = $panelGeneModel;
        $this->variantModel = $variantModel;
    }

    /**
     * Creates gene list datagrid
     *
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentGeneListDatagrid()
    {
        $grid = new DataGrid(null, 'geneList');

        $grid->setPrimaryKey(GeneModel::COL_ID);
        $grid->setDataSource($this->geneModel->listGenes());

        // Columns
        $grid->addColumnText(GeneModel::COL_ID, 'ID')
            ->setDefaultHide();

        $grid->addColumnText(GeneModel::COL_NAME, 'Název');
        $grid->addColumnText(GeneModel::COL_REFERENCE, 'Reference');
        $grid->addColumnText(GeneModel::COL_SYNONYMS, 'Synonyma');
        $grid->addColumnText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addColumnDateTime(GeneModel::COL_DATE_FROM, 'Datum od')
            ->setFormat('d.m.Y')
            ->setDefaultHide();
        $grid->addColumnDateTime(GeneModel::COL_DATE_TO, 'Datum do')
            ->setFormat('d.m.Y')
            ->setDefaultHide();

        // Actions
        $grid->addAction('edit', null, 'Gene:edit', ['id' => GeneModel::COL_ID])
            ->setTitle('Upravit')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('delete', null, 'geneDelete!', ['id' => GeneModel::COL_ID])
            ->setConfirm('Opravdu chcete gen %s odstranit?', GeneModel::COL_NAME)
            ->setTitle('Odstranit')
            ->setIcon('trash')
            ->setClass('danger ajax');

        // Filters
        $grid->addFilterText(GeneModel::COL_NAME, 'Název');
        $grid->addFilterText(GeneModel::COL_REFERENCE, 'Reference');
        $grid->addFilterText(GeneModel::COL_SYNONYMS, 'Synonyma');
        $grid->addFilterText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addFilterDateRange(GeneModel::COL_DATE_FROM, 'Datum od')
            ->setFormat('j. n. Y', 'd.m.yyyy');
        $grid->addFilterDateRange(GeneModel::COL_DATE_TO, 'Datum do')
            ->setFormat('j. n. Y', 'd.m.yyyy');


        return $grid;
    }

    /**
     * Handles gene deletion
     *
     * @param $id : id of gene to delete
     * @throws Nette\Application\AbortException
     */
    public function handleGeneDelete($id)
    {
        if (!$this->isAjax()) {
            $this->redirect('this');
        }

        if ($this->variantModel->getVariantByGeneId($id)->count() != 0) {
            $this->flashMessage("Nelze odstranit gen přiřazený k variantě!", 'danger');
            $this->redrawControl('flashes');
        } else if ($this->panelGeneModel->getPanelGeneByGeneId($id)->count() != 0) {
            $this->flashMessage("Nelze odstranit gen přiřazený k panelu!", 'danger');
            $this->redrawControl('flashes');
        } else {
            $result = $this->geneModel->deleteGeneById($id);

            if ($result) {
                $this->flashMessage("Gen byl úspěšně odstraněn!", 'success');
                $this->redrawControl('flashes');
                $this['geneListDatagrid']->reload();
            } else {
                $this->flashMessage("Gen se nepodařilo odstranit!", 'error');
                $this->redrawControl('flashes');
            }
        }
    }

    /**
     * Creates a form to delete the gene
     *
     * @return Form
     */
    protected function createComponentDeleteGeneForm()
    {
        $form = new Form;
        $form->addText('gen_id');
        $form->addSubmit('send');

        $form->onSuccess[] = [$this, 'deleteGeneSuccess'];

        return $form;
    }

    /**
     * Deletes the gene from database
     *
     * @param $form
     * @param $values
     */
    public function deleteGeneSuccess($form, $values)
    {
        $gene_id = $values['gen_id'];

        $result = $this->geneModel->deleteGeneById($gene_id);

        if ($result > 0) {
            $this->flashMessage('Gen byl smazán.', 'success');
        } else {
            $this->flashMessage('Nastala chyba při smazání genu.', 'danger');
        }
    }

    /**
     * Renders the page to edit the gene.
     *
     * @param $id
     */
    public function renderEdit($id)
    {

    }

    /**
     * Creates a form to add the gene
     *
     * @return Form
     */
    protected function createComponentGeneNewForm()
    {
        $form = $this->geneFormFactory->create();

        $form->addSubmit('getExtData', 'Najít gen pomocí Varsome')
            ->setHtmlAttribute('class', 'btn btn-primary')
            ->onClick[] = [$this, 'getExtGeneDataSuccess'];

        $form->addSubmit('saveData', 'Přidat gen')
            ->setHtmlAttribute('class', 'btn btn-success float-right')
            ->onClick[] = [$this, 'saveGeneSuccess'];

        return $form;
    }

    /**
     * Creates a form to edit the gene.
     *
     * @return Form
     */
    protected function createComponentGeneEditForm()
    {

        $form = $this->geneFormFactory->create();
        $form->addHidden('gen_id');

        $form->addText('gen_datum_od', 'Datum od')
            ->setHtmlAttribute('class', 'form-control')
            ->setAttribute('readonly', true);

        $form->addText('gen_datum_do', 'Datum do')
            ->setHtmlAttribute('class', 'form-control')
            ->setAttribute('readonly', true);

        $form->addSubmit('send', 'Upravit')
            ->setHtmlAttribute('class', 'btn btn-success float-right')
            ->onClick[] = [$this, 'editGeneSuccess'];

        $data = $this->geneModel->getGeneById($this->getParameter('id'))->fetch();
        $form->setDefaults($data);

        $form['gen_datum_od']->setDefaultValue(date('d.m.Y', strtotime($data->gen_datum_od)));

        if ($data->gen_datum_do != null) {
            $form['gen_datum_do']->setDefaultValue(date('d.m.Y', strtotime($data->gen_datum_do)));
            $form->addSubmit('closeGene', 'Odemknout referenční sekvenci')
                ->setHtmlAttribute('class', 'btn btn-primary')
                ->onClick[] = [$this, 'unlockGeneSuccess'];
        } else {
            $form->addSubmit('openGene', 'Uzamknout referenční sekvenci')
                ->setHtmlAttribute('class', 'btn btn-primary')
                ->onClick[] = [$this, 'lockGeneSuccess'];
        }

        return $form;
    }

    /**
     * Gets data from varsome api
     *
     * @param $form
     * @param $values
     */
    public function getExtGeneDataSuccess($form, $values)
    {

        $gene_symbol = trim($values['gen_nazev']);

        if (strlen($gene_symbol) == 0) {
            $this->flashMessage('Nebyl zadán název genu.', 'danger');
        } else {
            $varsome_api = new VarsomeAPI($this->context->parameters['varsome']['apikey']);
            $varsome_api = $varsome_api->getGeneLookup($values['gen_nazev'], null);

            if ($varsome_api[0]->getSymbol() == null) {
                $this->flashMessage('Gen nebyl nalezen.', 'danger');
            } else {
                $this->template->geneData = $varsome_api;
            }
        }

    }

    /**
     * Saves the gene to the database.
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function saveGeneSuccess($form, $values)
    {

        if (!strlen($values['gen_nazev']) > 0) {
            $this->flashMessage('Nebyl zadán název genu.', 'warning');
        } else if ($this->geneModel->getGeneByNameAndRefSequence($values['gen_nazev'],
                $values['gen_reference'])->fetch() != null) {

            $this->flashMessage('Gen s tímto názvem a referenční sekvencí existuje.', 'danger');
        } else if ($this->geneModel->getGeneByNameAndNullDateTo($values['gen_nazev'])->fetch() != null) {
            $this->flashMessage('Otevřený gen s tímto názvem už existuje.', 'danger');
        } else {

            if (strlen($values['gen_reference']) == 0) {
                $values['gen_nazev'] = null;
            }

            $values->gen_datum_od = date("Y-m-d");
            $result = $this->geneModel->insertGene($values);

            if ($result) {
                $this->flashMessage('Gen byl přidán.', 'success');
                $this->redirect('Gene:new');
            } else {
                $this->flashMessage('Nastala chyba při přidávání genu.', 'danger');
            }
        }

        $this->redrawControl('flashes');
    }

    /**
     * Edits the gene
     *
     * @param $form
     * @param $values
     */
    public function editGeneSuccess($form, $values)
    {
        $result = null;
        if (!strlen($values['gen_nazev']) > 0) {
            $this->template->geneAlert = 'Nebyl zadán název genu.';
            $this->template->geneAction = 'warning';
        } else {
            $gene = $this->geneModel->getGeneById($values['gen_id'])->fetch();
            if (strcmp($gene->gen_nazev, $values['gen_nazev']) == 0 &&
                strcmp($gene->gen_reference, $values['gen_reference']) == 0) {

                $gene_id = $values['gen_id'];
                $values['gen_datum_od'] = $gene->gen_datum_od;
                $values['gen_datum_do'] = $gene->gen_datum_do;
                unset($values['gen_id']);
                $result = $this->geneModel->updateGeneById($gene_id, $values);
            } else if ($this->geneModel->getGeneByNameAndRefSequence($values['gen_nazev'],
                    $values['gen_reference'])->fetch() != null) {

                $this->flashMessage('Gen s tímto názvem a referenční sekvencí existuje.', 'danger');
            } else if ($gene->gen_datum_do == null &&
                $this->geneModel->getGeneByNameAndNullDateTo($values['gen_nazev'])->fetch() != null) {

                $this->flashMessage('Už existuje otevřený gen s tímto jménem.', 'danger');
            } else {
                $gene_id = $values['gen_id'];
                $values['gen_datum_od'] = $gene->gen_datum_od;
                $values['gen_datum_do'] = $gene->gen_datum_do;
                unset($values['gen_id']);
                $result = $this->geneModel->updateGeneById($gene_id, $values);
            }
        }
        if ($result != null) {
            if ($result) {
                $this->flashMessage('Gen byl editován.', 'success');
            } else {
                $this->flashMessage('Nastala chyba při editování genu.', 'danger');
            }
        }
    }

    /**
     *  Creates a form to save external gene data.
     *
     * @return Form
     */
    protected function createComponentGeneSaveForm()
    {
        $form = $this->geneFormFactory->create();
        $form->addSubmit('saveData', 'Vybrat gen')
            ->setHtmlAttribute('class', 'form-control btn btn-success')
            ->onClick[] = [$this, 'saveGeneSuccess'];

        return $form;
    }

    /**
     * Locks a gene (adds date to)
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function lockGeneSuccess($form, $values)
    {
        $gene_id = $values['gen_id'];
        $gene = $this->geneModel->getGeneById($gene_id)->fetch();

        $newGene = array(
            "gen_nazev" => $gene->gen_nazev,
            "gen_reference" => $gene->gen_reference,
            "gen_popis" => $gene->gen_popis,
            "gen_synonyma" => $gene->gen_synonyma,
            "gen_datum_od" => $gene->gen_datum_od,
            "gen_datum_do" => date("Y-m-d")
        );

        $result = $this->geneModel->updateGeneById($gene_id, $newGene);

        if ($result) {
            $this->flashMessage('Gen byl uzamčen.', 'success');
            $this->redirect('Gene:edit', $gene_id);
        } else {
            $this->flashMessage('Nastala chyba při uzamčení genu.', 'danger');
        }

    }

    /**
     * Unlocks a gene (removes date to)
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function unlockGeneSuccess($form, $values)
    {
        $gene_id = $values['gen_id'];
        $gene = $this->geneModel->getGeneById($gene_id)->fetch();

        if ($this->geneModel->getGeneByNameAndNullDateTo($gene->gen_nazev)->fetch() != null) {
            $this->flashMessage('Nelze mít otevřených více genů se stejným názvem.', 'danger');
        } else {
            $newGene = array(
                "gen_nazev" => $gene->gen_nazev,
                "gen_reference" => $gene->gen_reference,
                "gen_popis" => $gene->gen_popis,
                "gen_synonyma" => $gene->gen_synonyma,
                "gen_datum_od" => $gene->gen_datum_od,
                "gen_datum_do" => null
            );

            $result = $this->geneModel->updateGeneById($gene_id, $newGene);

            if ($result) {
                $this->flashMessage('Gen byl odemčen.', 'success');
                $this->redirect('Gene:edit', $gene_id);
            } else {
                $this->flashMessage('Nastala chyba při odemčení genu.', 'danger');
            }

        }

    }

}