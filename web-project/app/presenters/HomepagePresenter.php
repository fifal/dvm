<?php

namespace App\Presenters;

use App\Model\EmployeeModel;
use Nette;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context */
    public $context;

    /** @var EmployeeModel @inject */
    public $employeeModel;

    public function __construct(Nette\Database\Context $context)
    {
        $this->context = $context;
    }

    /**
     * Renders default page
     *     - if there is an error when fetching from table employee
     *          -> database wasn't initialized -> redirect to Install
     *     - if there is 0 users
     *          -> admin wasn't created -> redirect to Install
     *
     * @throws Nette\Application\AbortException
     */
    public function renderDefault()
    {
        try
        {
            $employees = $this->employeeModel->listEmployees();

            if ($employees->count() == 0)
            {
                $this->redirect('Install:default');
            }
        } catch (\Exception $exception)
        {
            $this->redirect('Install:default');
        }
    }
}
