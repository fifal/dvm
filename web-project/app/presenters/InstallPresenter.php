<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 03.07.2018
 * Time: 14:11
 */

namespace App\Presenters;


use App\Components\BootstrapForm;
use App\Components\UserEditForm;
use App\Model\AdminResetModel;
use App\Model\Auth\AuthenticatorInstall;
use App\Model\EmployeeModel;
use App\Model\RoleModel;
use App\Utils\ImportSql;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Utils\Random;

class InstallPresenter extends Presenter
{
    /** @var Context @inject */
    public $database;

    /** @var EmployeeModel @inject */
    public $employeeModel;

    /** @var RoleModel @inject */
    public $roleModel;

    /** @var UserEditForm @inject */
    public $userFormFactory;

    /** @var AdminResetModel @inject */
    public $adminResetModel;

    public function startup()
    {
        parent::startup();

        if ($this->getUser()->isLoggedIn() && !$this->getUser()->isInRole('install'))
        {
            $this->getUser()->logout();
        }

        if (!$this->getUser()->isLoggedIn())
        {
            $this->loginInstallUser();
        }

        if (!$this->getUser()->isAllowed('installation'))
        {
            $this->flashMessage('Ke konfiguraci aplikace je přístup povolen pouze pokud v databázi neexistuje žádný uživatel!', 'error');
            $this->redirect('Homepage:default');
        }
    }

    //
    //
    // ACTIONS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * After clicking on button "Dokončit konfiguraci" -> logout user and redirect to login page
     *
     * @throws \Nette\Application\AbortException
     */
    public function actionComplete()
    {
        $this->getUser()->logout();

        $this->flashMessage('Konfigurace byla úspěšně dokončena, nyní se můžete přihlásit.', 'success');
        $this->redirect('User:login');
    }

    /**
     * Redirects user to create database if it wasn't initialized or to create admin if there is no user in application
     *
     * @throws \Nette\Application\AbortException
     */
    public function actionDefault()
    {
        if (!$this->doesDbExist())
        {
            $this->flashMessage('V databázi nebyla nalezena žádná tabulka.', 'danger');
            $this->redirect('Install:createDatabase');
        }

        $employeeCount = $this->employeeModel->listEmployees()->count();
        if ($employeeCount == 0)
        {
            $this->flashMessage('V databázi nebyl nalezen žádný uživatel, vytvořte prosím administrátorský účet.', 'danger');
            $this->redirect('Install:createAdmin');
        }
    }

    //
    //
    // RENDERS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows page with generated tokens
     */
    public function renderTokens()
    {
        $tokenName = 'admin_reset_heslo';
        $tokenUsed = 'admin_reset_pouzito';

        $tokens = array();
        for ($i = 0; $i < 10; $i++)
        {
            array_push($tokens, Random::generate(32));
        }

        $this->template->tokens = $tokens;

        $data = array();

        foreach ($tokens as $token)
        {
            $tokenArray = array();
            $tokenArray[$tokenName] = $token;
            $tokenArray[$tokenUsed] = 0;
            array_push($data, $tokenArray);
        }

        //TODO: kontrola chyb
        // In case user refreshes page, generate new tokens and delete old ones from database
        $this->adminResetModel->deleteAllTokens();
        $this->adminResetModel->addTokens($data);
    }


    //
    //
    // ADD ADMIN ACCOUNT
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates form for admin creation
     *
     * @return Form: UserEditForm
     */
    protected function createComponentAddAdminForm()
    {
        $form = $this->userFormFactory->create();

        // Login is required
        $form['vysetrujici_login']
            ->setRequired('Pole uživatelské jméno je povinné.');

        $roleId = $this->roleModel->getRoleByName('admin')->fetch()->role_id;
        // Disable role
        $form['vysetrujici_role']->setDisabled(true);
        $form['vysetrujici_role']->setValue($roleId);

        // Add password and password check
        $form->addPassword('vysetrujici_password', 'Heslo')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired('Pole Heslo je povinné.')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít minimálně 4 znaky.', 4);

        $form->addPassword('passwordConfirm', 'Heslo znovu')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired('Pole Heslo znovu je povinné.')
            ->addRule(Form::EQUAL, 'Vaše vyplněné heslo se neshoduje.', $form['vysetrujici_password']);

        // Submit button
        $form->addSubmit('send', 'Vytvořit administrátorský účet')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'addAdminSuccess'];

        return $form;
    }

    /**
     * Handles admin creation form and creates administrator in database
     *
     * @param $form
     * @param $values
     * @throws \Nette\Application\AbortException
     */
    public function addAdminSuccess($form, $values)
    {
        $password = $values['vysetrujici_password'];
        $passwordConfirm = $values['passwordConfirm'];

        // Should never be true, but just in case
        if ($password != $passwordConfirm)
        {
            $this->setAdminAlert('Hesla se neshodují!', 'error');
            return;
        }

        // Unset password confirmation
        unset($values['passwordConfirm']);

        // Create data array
        $values['vysetrujici_password'] = Passwords::hash($password);

        // Remove empty values
        foreach ($values as $key => $value)
        {
            if (empty($value))
            {
                unset($values[$key]);
            }
        }

        // Get admin role ID
        $roles = $this->roleModel->getRoleByName('admin')->fetch();
        $values['vysetrujici_role'] = $roles->role_id;

        // Insert admin into database
        //TODO: zkontrolovat result
        $this->employeeModel->insertEmployee($values);

        $this->redirect('Install:tokens');
    }

    /**
     * Sets alert in add admin form
     *
     * @param $alert : message
     * @param $type : type of alert (success, danger, error, ...)
     */
    public function setAdminAlert($alert, $type)
    {
        $this->template->addAdminAlert = $alert;
        $this->template->addAdminAction = $type;
    }

    //
    //
    // CREATE DATABASE
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Renders create database page which shows name of connected database
     * @throws ForbiddenRequestException
     */
    public function renderCreateDatabase()
    {
        if ($this->doesDbExist())
        {
            if ($this->getUser()->isLoggedIn())
            {
                $this->getUser()->logout();
            }

            throw new ForbiddenRequestException('Do této části nemáte přístup!');
        }

        $this->template->database = $this->context->parameters['database']['database'];
    }

    /**
     * Creates submit button
     *
     * @return Form
     */
    protected function createComponentCreateDbForm()
    {
        $form = new BootstrapForm;

        $form->addSubmit('send', 'Vytvořit databázi')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'createDbSuccess'];

        return $form;
    }


    /**
     * Handles button from create db form and creates database
     *
     * @param $form
     * @param $values
     * @throws \Nette\Application\AbortException
     */
    public function createDbSuccess($form, $values)
    {
        $this->createDatabaseTables();

        $this->flashMessage('Databáze byla úspěšně vytvořena', 'success');
        $this->redirect('Install:createAdmin');
    }

    //
    //
    // UTILS
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if database tables were initialized, returns false otherwise
     *
     * @return bool
     */
    private function doesDbExist()
    {
        try
        {
            $employeeCount = $this->employeeModel->listEmployees()->count();
            return true;
        } catch (\Exception $exception)
        {
            return false;
        }
    }


    /**
     * Login for temp install user with role "install"
     *
     * @throws \Nette\Security\AuthenticationException
     */
    public function loginInstallUser()
    {
        $auth = new AuthenticatorInstall();
        $this->getUser()->setAuthenticator($auth);

        try
        {
            $employeeCount = $this->employeeModel->listEmployees()->count();
        } catch (\Exception $exception)
        {
            $employeeCount = -1;
        }

        if (!$this->doesDbExist() || $employeeCount == 0)
        {
            $this->getUser()->login(array());
        }
    }

    /**
     * Creates database tables
     */
    public function createDatabaseTables()
    {
        $sqlScript = ImportSql::getSqlScript($this->context->parameters['database']['database']);
        $this->database->query($sqlScript);
    }
}