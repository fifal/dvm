<?php

namespace App\Presenters;

use App\Components\BootstrapForm;
use App\Model\GeneModel;
use App\Model\PanelModel;
use App\Model\PanelGeneModel;
use App\Utils\DataGrid\DataGrid;
use Nette\Application\UI\Form;

use Nette;

class PanelPresenter extends Nette\Application\UI\Presenter
{

    /** @var PanelModel */
    private $panelModel;

    /** @var PanelGeneModel */
    private $panelGeneModel;

    /** @var GeneModel */
    private $geneModel;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * PanelPresenter constructor.
     * @param PanelModel $panelModel
     * @param PanelGeneModel $panelGeneModel
     * @param GeneModel $geneModel
     */
    public function __construct(PanelModel $panelModel, PanelGeneModel $panelGeneModel, GeneModel $geneModel)
    {
        $this->panelModel = $panelModel;
        $this->panelGeneModel = $panelGeneModel;
        $this->geneModel = $geneModel;
    }

    /**
     * Creates a form to create a panel
     *
     * @return Form
     */
    protected function createComponentPanelNewForm()
    {
        $form = new BootstrapForm;

        $form->addText('panel_nazev', 'Název')
            ->setHtmlAttribute('class', 'form-control')
            ->addRule(Form::FILLED, 'Musíte zadat jméno panelu');

        $form->addText('panel_poznamka', 'Poznámka')
            ->setHtmlAttribute('class', 'form-control');

        $form->addSubmit('send', 'Přidat panel')
            ->setHtmlAttribute('class', 'form-control btn btn-success mt-3');

        $form->onSuccess[] = [$this, 'panelNewSuccess'];

        return $form;
    }

    /**
     * Creates a panel
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function panelNewSuccess($form, $values)
    {

        if (!strlen($values['panel_nazev']) > 0) {
            $this->flashMessage('Nebyl zadán název panelu.', 'danger');
        }
        else if($this->panelModel->getPanelByName($values['panel_nazev'])->fetch() != null){
            $this->flashMessage('Panel s tímto názvem už existuje.', 'danger');
        }
        else {

            $result = $this->panelModel->insertPanel($values);

            if ($result) {
                $this->flashMessage('Panel byl přidán.', 'success');
                $this->redirect('Panel:detail', ['id' => $result]);
            } else {
                $this->flashMessage('Nastala chyba při přidání panelu.', 'danger');
            }
        }

    }

    /**
     * Renders detail of a panel
     *
     * @param $id
     */
    public function renderDetail($id){

    }

    /**
     * Creates Datagrid for removing genes from panel
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentGenesListDataGrid()
    {
        $grid = new DataGrid(null, 'genesListDataGrid');

        $grid->setPrimaryKey(GeneModel::COL_ID);

        $panelId = $this->getParameter('id') != null ? $this->getParameter('id') : 0;
        $genesInPanel = $this->panelGeneModel->getGenesIdsInPanel($panelId);

        if(count($genesInPanel) == 0){
            $genesInPanel = null;
        }

        $grid->setDataSource(
            $this->geneModel->database->table(GeneModel::TABLE_NAME)->where(GeneModel::COL_ID, $genesInPanel)
        );

        // Columns
        $grid->addColumnText(GeneModel::COL_ID, 'ID')
            ->setDefaultHide();
        $grid->addColumnText(GeneModel::COL_NAME, 'Název');
        $grid->addColumnText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addColumnText(GeneModel::COL_SYNONYMS, 'Synonyma');

        // Filters
        $grid->addFilterText(GeneModel::COL_NAME, 'Název');
        $grid->addFilterText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addFilterText(GeneModel::COL_SYNONYMS, 'Synonyma');

        // Action
        $grid->addAction('edit', null, 'Gene:edit', ['id' => GeneModel::COL_ID])
            ->setTitle('Upravit gen')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('remove', null, 'removeGenesFromPanel!', ['ids' => GeneModel::COL_ID])
            ->setTitle('Odebrat gen z panelu')
            ->setIcon('trash')
            ->setClass('danger ajax');

        // Group Action
        $grid->addGroupAction('Odstranit gen z panelu')->onSelect[] = [$this, 'handleRemoveGenesFromPanel'];

        return $grid;
    }

    /**
     * Creates Datagrid for assigning genes to panel
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentGenesDataGrid()
    {
        $grid = new DataGrid(null, 'genesDataGrid');

        $grid->setPrimaryKey(GeneModel::COL_ID);
        $grid->setDataSource($this->geneModel->listGenes());

        // Columns
        $grid->addColumnText(GeneModel::COL_ID, 'ID')
            ->setDefaultHide();
        $grid->addColumnText(GeneModel::COL_NAME, 'Název');
        $grid->addColumnText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addColumnText(GeneModel::COL_SYNONYMS, 'Synonyma');

        // Filters
        $grid->addFilterText(GeneModel::COL_NAME, 'Název');
        $grid->addFilterText(GeneModel::COL_DESCRIPTION, 'Popis');
        $grid->addFilterText(GeneModel::COL_SYNONYMS, 'Synonyma');

        // Action
        $grid->addAction('edit', null, 'Gene:edit', ['id' => GeneModel::COL_ID])
            ->setTitle('Upravit gen')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('add', null, 'addGenesToPanel!', ['ids' => GeneModel::COL_ID])
            ->setTitle('Přiřadit gen k panelu')
            ->setIcon('plus')
            ->setClass('success ajax');

        // Group Action
        $grid->addGroupAction('Přiřadit gen k panelu')->onSelect[] = [$this, 'handleAddGenesToPanel'];

        return $grid;
    }

    /**
     * Handles assigning genes to panel
     *
     * @param array $ids
     * @throws Nette\Application\AbortException
     */
    public function handleAddGenesToPanel($ids)
    {
        $count = 0;
        $panelId = $this->getParameter('id');

        if (!is_array($ids))
        {
            $tmp = $ids;
            unset($ids);
            $ids[] = $tmp;

        }

        foreach ($ids as $id)
        {
            try
            {
                if ($this->panelGeneModel->insertPanelGene(
                    [
                        PanelGeneModel::COL_PANEL_ID => $panelId,
                        PanelGeneModel::COL_GEN_ID => $id
                    ]
                ))
                {
                    $count++;
                }
            } catch (\Exception $ex)
            {
                \Tracy\Debugger::log('Chyba při přidávání genů k panelu: ' . $ex->getMessage(), 'database.error');
                $this->flashMessage("Gen s ID $id se nepodařilo přidat.", 'danger');
                continue;
            }
        }


        $this->flashMessage('Počet úspěšně přiřazených genů: ' . $count, 'success');
        $this->redrawControl('flashes');
        $this['genesListDataGrid']->reload();
    }

    /**
     * Handles removing genes from panel
     *
     * @param array $ids
     * @throws Nette\Application\AbortException
     */
    public function handleRemoveGenesFromPanel($ids){
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $count = 0;
        $panelId = $panelId = $this->getParameter('id');


        if (!is_array($ids))
        {
            $tmp = $ids;
            unset($ids);
            $ids[] = $tmp;
        }

        foreach ($ids as $id)
        {
            try
            {
                if ($this->panelGeneModel->deletePanelGeneByPK($panelId, $id))
                {
                    $count++;
                }
            } catch (\Exception $ex)
            {
                \Tracy\Debugger::log('Chyba při odstraňování genů z panelu: ' . $ex->getMessage(), 'database.error');
                $this->flashMessage("Gen s ID $id se nepodařilo odstranit.", 'danger');
            }
        }
        $this->flashMessage('Počet úspěšně odebraných genů: ' . $count, 'success');
        $this->redrawControl('flashes');
        $this->redirect('this');

    }

    /**
     * Creates Datagrid for list of panels
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentPanelListDataGrid()
    {
        $grid = new DataGrid(null, 'panelListDataGrid');

        $grid->setPrimaryKey(PanelModel::COL_ID);

        $grid->setDataSource($this->panelModel->getListOfPanels());

        // Columns
        $grid->addColumnText(PanelModel::COL_ID, 'ID')
            ->setDefaultHide();
        $grid->addColumnText(PanelModel::COL_NAME, 'Název');
        $grid->addColumnText(PanelModel::COL_NOTE, 'Poznámka');

        // Filters
        $grid->addFilterText(PanelModel::COL_NAME, 'Název');
        $grid->addFilterText(PanelModel::COL_NOTE, 'Poznámka');

        // Action
        $grid->addAction(PanelModel::COL_ID, null, 'Panel:detail', ['id' => PanelModel::COL_ID])
            ->setTitle('Upravit')
            ->setIcon('pencil-alt')
            ->setClass('success');

        return $grid;
    }

    /**
     * Creates a form to create a panel
     *
     * @return Form
     */
    protected function createComponentPanelEditForm()
    {
        $form = new BootstrapForm;

        $form->addHidden('panel_id');

        $form->addText('panel_nazev', 'Název')
            ->setHtmlAttribute('class', 'form-control');

        $form->addText('panel_poznamka', 'Poznámka')
            ->setHtmlAttribute('class', 'form-control');

        $form->addSubmit('editPanel', 'Upravit')
            ->setHtmlAttribute('class', 'btn btn-success float-right ml-1')
            ->onClick[] = [$this, 'panelEditSuccess'];

        $form->addSubmit('deletePanel', 'Smazat')
            ->setHtmlAttribute('class', 'btn btn-danger float-right')
            ->onClick[] = [$this, 'panelDeleteSuccess'];

        $data = $this->panelModel->getPanelById($this->getParameter('id'))->fetch();
        $form->setDefaults($data);

        return $form;
    }

    /**
     * Edits a panel
     *
     * @param $form
     * @param $values
     */
    public function panelEditSuccess($form, $values)
    {
        $result = false;

        if (!strlen($values['panel_nazev']) > 0)
        {
            $this->flashMessage('Nebyl zadán název panelu.', 'danger');
        } else
        {
            $panel = $this->panelModel->getPanelById($values['panel_id'])->fetch();
            if (strcmp($panel->panel_nazev, $values['panel_nazev']) == 0)
            {
                $panel_id = $values['panel_id'];
                unset($values['panel_id']);
                $result = $this->panelModel->updatePanelById($panel_id, $values);
            } else if ($this->panelModel->getPanelByName($values['panel_nazev'])->fetch() != null)
            {
                $this->flashMessage('Panel s tímto názvem už existuje.', 'danger');
            } else
            {
                $panel_id = $values['panel_id'];
                unset($values['panel_id']);
                $result = $this->panelModel->updatePanelById($panel_id, $values);
            }
        }

        if ($result)
        {
            $this->flashMessage('Panel byl upraven.', 'success');
        } else
        {
            $this->flashMessage('Nastala chyba při úpravě panelu.', 'danger');
        }

    }

    /**
     * Deletes a panel
     *
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function panelDeleteSuccess($form, $values)
    {
        $panel_id = $values['panel_id'];
        $this->panelGeneModel->deletePanelGeneByPanelId($panel_id);
        $this->panelModel->deletePanelById($panel_id);
        $this->flashMessage('Panel byl smazán.', 'success');
        $this->redirect('Panel:list');
    }

}