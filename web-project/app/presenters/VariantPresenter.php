<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 06.08.2018
 * Time: 22:12
 */

namespace App\Presenters;


use App\Components\BootstrapForm;
use App\Components\VariantAddForm;
use App\Components\VariantGrid;
use App\Model\Cron\UpdateVariantsFromVarsome;
use App\Model\Cron\UpdateVariantsGeneCommand;
use App\Model\GeneModel;
use App\Model\VariantExamModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;
use App\Utils\Varsome\VarsomeAPI;
use App\Utils\Varsome\VarsomeVariant;
use Kdyby\Console\StringOutput;
use Nette\Application\UI\Multiplier;
use Nette\Application\UI\Presenter;
use Nette\Forms\Form;
use Nette\Utils\DateTime;
use Symfony\Component\Console\Input\StringInput;

class VariantPresenter extends Presenter
{
    /** @var VariantModel @inject */
    public $variantModel;

    /** @var GeneModel @inject */
    public $geneModel;

    /** @var UpdateVariantsGeneCommand @inject */
    public $updateVariantsGene;

    /** @var UpdateVariantsFromVarsome @inject */
    public $updateVariantsVarsome;

    /** @var VariantAddForm @inject */
    public $variantAddFormFactory;

    /** @var VariantGrid @inject */
    public $variantGridFactory;

    /** @var VarsomeAPI @inject */
    public $varsomeApi;

    /** @var  VariantExamModel @inject */
    public $variantExamModel;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * Creates Variant data grid
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentVariantsDataGrid()
    {
        $grid = $this->variantGridFactory->create();
        // Toolbar
        $grid->addToolbarButton('updateVariantGene!', 'Přiřadit geny')
            ->setClass('btn btn-xs btn-success ajax')
            ->setTitle('Přiřadí ke všem variantám gen – podle prefixu před dvojtečkou v HGVS');
        $grid->addToolbarButton('updateVariantVarsome!', 'Aktualizovat z Varsome')
            ->setClass('btn btn-xs btn-success ajax')
            ->setTitle('Aktualizuje Clinvar sloupečky pomocí Varsome');

        // Actions
        $grid->addAction('edit', null, 'Variant:edit', ['id' => VariantModel::COL_ID])
            ->setTitle('Upravit variantu')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('delete', null, 'deleteVariant!', ['id' => VariantModel::COL_ID])
            ->setConfirm('Opravdu chcete variantu %s odstranit?', VariantModel::COL_RS)
            ->setTitle('Odstranit')
            ->setIcon('trash')
            ->setClass('danger ajax');

        return $grid;
    }

    /**
     * Assigns gen into variant if possible
     *
     * @throws \Exception
     */
    public function handleUpdateVariantGene()
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $input = new StringInput('');
        $output = new StringOutput();

        $this->updateVariantsGene->run($input, $output);

        // Redraw controls
        $this->flashMessage($output->getOutput(), 'success');
        $this->redrawControl('flashes');
        $this['variantsDataGrid']->reload();
    }

    /**
     * Updates Clinvar parameters for each variant from Varsome Api
     *
     * @throws \Nette\Application\AbortException
     */
    public function handleUpdateVariantVarsome()
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $input = new StringInput('');
        $output = new StringOutput();

        $this->updateVariantsVarsome->run($input, $output);

        // Redraw controls
        $this->flashMessage($output->getOutput(), 'success');
        $this->redrawControl('flashes');
        $this['variantsDataGrid']->reload();
    }

    /**
     * Creates form for Variants adding
     *
     * @return Multiplier
     */
    public function createComponentVariantAddForm()
    {
        return new Multiplier(function ()
        {
            $form = $this->variantAddFormFactory->create();

            $form->addSubmit('search', 'Vyhledat ve Varsome')
                ->setHtmlAttribute('class', 'btn btn-primary ajax');
            $form->addSubmit('add', 'Přidat variantu')
                ->setHtmlAttribute('class', 'btn btn-success ajax');

            $form->onSuccess[] = [$this, 'handleVariant'];
            return $form;
        });
    }

    /**
     * Form for variant edit
     *
     * @return BootstrapForm
     */
    public function createComponentVariantEditForm()
    {
        $form = $this->variantAddFormFactory->create();

        $variantId = $this->getParameter('id');
        $form->addHidden(VariantModel::COL_ID, $variantId);

        $form->addSubmit('update', 'Aktualizovat z Varsome')
            ->setHtmlAttribute('class', 'btn btn-primary ajax');

        $form->addSubmit('edit', 'Upravit variantu')
            ->setHtmlAttribute('class', 'btn btn-success ajax');

        $variant = $this->variantModel->getVariantById($variantId)->fetch();

        if ($variant)
        {
            $data = $variant->toArray();

            // Format date to d.m.Y before showing in Form
            if (isset ($data[VariantModel::COL_CLINVAR_DATE]))
            {
                $clinvarDate = $data[VariantModel::COL_CLINVAR_DATE];
                if (!$clinvarDate instanceof DateTime)
                {
                    $clinvarDate = new DateTime($clinvarDate);
                }

                $data[VariantModel::COL_CLINVAR_DATE] = $clinvarDate->format('d.m.Y');
            }

            $form->setDefaults($data);
        }

        $form->onSuccess[] = [$this, 'handleEditVariant'];
        return $form;
    }

    /**
     * Handles Variant form submit
     * @param $form
     * @throws \Nette\Application\AbortException
     */
    // TODO: referenční geny
    public function handleVariant(Form $form)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        // Form was submitted by search button
        if ($form['search']->isSubmittedBy())
        {
            $this->searchAction($form);
        }

        // Form was submitted by add button
        if ($form['add']->isSubmittedBy())
        {
            $this->addAction($form->getValues(true), false);
        }

        $this->redrawControl('flashes');
        $this->redrawControl('variantsSnippet');
    }

    /**
     * Handles search button of variant add form,
     *
     * @param Form $form
     */
    private function searchAction(Form $form)
    {
        $values = $form->getValues();

        // If RS is not empty
        $result = $this->variantVarsomeLookup($values);

        /** @var VarsomeVariant $result [0] */
        if (!isset($result) || $result[0]->getRsid() === null)
        {
            $this->template->varsomeNotFound = true;
        } else
        {
            $this->template->varsomeVariants = $result;
        }
    }

    /**
     * Handles add button from Varsome table
     *
     * @param array $values
     * @throws \Nette\Application\AbortException
     */
    public function handleAdd(array $values)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $this->addAction($values, true);
        $this->redrawControl('flashes');
    }

    /**
     * Adds new variant into database
     *
     * @param array $values
     * @param $fromVarsome true -> inserting from Varsome
     */
    private function addAction(array $values, $fromVarsome)
    {
        // Remove rs and convert to int
        $values[VariantModel::COL_RS] = str_replace('rs', '', $values[VariantModel::COL_RS]);
        $values[VariantModel::COL_RS] = intval($values[VariantModel::COL_RS]);

        if (!$values[VariantModel::COL_CLINVAR_DATE] instanceof \DateTime && !empty($values[VariantModel::COL_CLINVAR_DATE]))
        {
            $values[VariantModel::COL_CLINVAR_DATE] = new DateTime($values[VariantModel::COL_CLINVAR_DATE]['date']);
        }

        if (!$fromVarsome)
        {
            unset($values[VariantModel::COL_CLINVAR_VERDICT]);
            unset($values[VariantModel::COL_CLINVAR_DATE]);
        }

        $variantExists = $this->variantModel->getVariantByRs($values[VariantModel::COL_RS])->count();
        if ($variantExists)
        {
            $this->flashMessage('Varianta se zadaným RS již v databázi existuje.', 'danger');
            return;
        }

        $result = $this->variantModel->insertVariant($values);
        if ($result)
        {
            $this->flashMessage('Varianta byla úspěšně přidána do databáze.', 'success');
        } else
        {
            $this->flashMessage('Variantu se nepodařilo přidat, zkuste to prosím později.', 'error');
        }
    }

    /**
     * Handles deletion of variant
     *
     * @param $id
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteVariant($id)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        if($this->variantExamModel->getVarExamByVarId($id)->count() != 0){
            $this->flashMessage('Nelze smazat variantu přiřazenou k vyšetření.', 'danger');
            $this->redrawControl('flashes');
            return;
        }

        if ($this->variantModel->deleteVariantById($id))
        {
            $this->flashMessage('Varianta byla úspěšně odstraněna.', 'success');
        } else
        {
            $this->flashMessage('Variantu se nepodařilo smazat.', 'error');
        }

        $this->redrawControl('flashes');
        $this['variantsDataGrid']->reload();
    }

    /**
     * Handles variant edit
     *
     * @param Form $form
     * @throws \Nette\Application\AbortException
     */
    public function handleEditVariant(Form $form)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $values = $form->getValues();
        $variant = $this->variantModel->getVariantById($values[VariantModel::COL_ID])->fetch();

        // Form was submitted by edit button
        if ($form['edit']->isSubmittedBy())
        {
            unset($values[VariantModel::COL_ID]);
            $result = $variant->update($values);
        }

        // Form was submitted by update button
        if ($form['update']->isSubmittedBy())
        {
            $varsome = $this->variantVarsomeLookup($values);
            $bestStars = -1;
            $bestVariant = null;

            /** @var VarsomeVariant $item */
            foreach ($varsome as $item)
            {
                if ($item->getClinvarVerdictStars() > $bestStars)
                {
                    $bestStars = $item->getClinvarVerdictStars();
                    $bestVariant = $item;
                }
            }

            if ($bestVariant == null)
            {
                $result = 0;
            } else
            {
                $data =
                    [
                        VariantModel::COL_CLINVAR_VERDICT => $bestVariant->getClinvarVerdict(),
                        VariantModel::COL_CLINVAR_DATE => $bestVariant->getLastEvalDate()
                    ];

                $result = $this->variantModel->updateVariantById($variant[VariantModel::COL_ID], $data);
            }
        }

        if ($result)
        {
            $variant = $this->variantModel->getVariantById($values[VariantModel::COL_ID])->fetch();
            $this['variantEditForm']->setValues($variant, true);
            $this->flashMessage('Varianta byla úspěšně upravena.', 'success');
        } else
        {
            $this->flashMessage('Variantu se nepodařilo upravit.', 'error');
        }

        $this->redrawControl('flashes');
        $this->redrawControl('editForm');
    }

    /**
     * Returns result from Varsome API by submitted form values
     *
     * @param $formValues
     * @return array
     */
    public function variantVarsomeLookup($formValues)
    {
        $result = array();

        if (!empty($formValues[VariantModel::COL_RS]))
        {
            $rs = $formValues[VariantModel::COL_RS];
            // If input doesn't contain rs at start add it
            if (strpos($rs, 'rs') === false)
            {
                $rs = 'rs' . $rs;
            }

            $result = $this->varsomeApi->getVariantLookup($rs);
        } elseif (!empty($formValues[VariantModel::COL_HGVS]))
        {
            $variant = str_replace(' ', '', $formValues[VariantModel::COL_HGVS]);
            $result = $this->varsomeApi->getVariantLookup($variant);
        }

        return $result;
    }
}