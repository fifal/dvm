<?php

namespace App\Utils\DataGrid;

use Nette\Database\Table\Selection;
use Ublaboo\DataGrid\DataGrid as BaseDataGrid;
use Ublaboo\DataGrid\Filter\FilterText;

/**
 * Class DataGrid provides DataGrid with Czech Translation, Bootstrap 4 template and customized
 * filter search using LIKE keyword in SQL query.
 * @package App\Utils\DataGrid
 */
class DataGrid extends BaseDataGrid
{
    /**
     * DataGrid constructor.
     * @param \Nette\ComponentModel\IContainer|null $parent
     * @param null $name
     */
    public function __construct(\Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($parent, $name);

        $translator = new \Ublaboo\DataGrid\Localization\SimpleTranslator([
            'ublaboo_datagrid.no_item_found_reset' => 'Žádné položky nenalezeny. Filtr můžete vynulovat',
            'ublaboo_datagrid.no_item_found' => 'Žádné položky nenalezeny.',
            'ublaboo_datagrid.here' => 'zde',
            'ublaboo_datagrid.items' => 'Položky',
            'ublaboo_datagrid.all' => 'všechny',
            'ublaboo_datagrid.from' => 'z',
            'ublaboo_datagrid.reset_filter' => 'Resetovat filtr',
            'ublaboo_datagrid.group_actions' => 'Hromadné akce',
            'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
            'ublaboo_datagrid.hide_column' => 'Skrýt sloupec',
            'ublaboo_datagrid.action' => 'Akce',
            'ublaboo_datagrid.previous' => 'Předchozí',
            'ublaboo_datagrid.next' => 'Další',
            'ublaboo_datagrid.choose' => 'Vyberte',
            'ublaboo_datagrid.execute' => 'Provést',
            'ublaboo_datagrid.show_default_columns' => 'Zobrazit výchozí sloupce',
            'ublaboo_datagrid.filter_submit_button' => 'Filtrovat',

            'Name' => 'Jméno',
            'Inserted' => 'Vloženo'
        ]);

        $this->setTranslator($translator);

        $this->setTemplateFile(__DIR__ . '/templates/datagrid.latte');

        $this->setColumnsHideable();

        $this->setAutoSubmit(false);

        $this->setStrictSessionFilterValues(false);
    }

    /**
     * Makes every column sortable by default
     *
     * @param string $key : column name
     * @param string $name : filter label
     * @param null $column
     * @return null|\Ublaboo\DataGrid\Column\ColumnText
     */
    public function addColumnText($key, $name, $column = null)
    {
        $column = parent::addColumnText($key, $name, $column);
        $column->setSortable();

        return $column;
    }

    /**
     * Makes every column sortable by default
     *
     * @param string $key : column name
     * @param string $name : filter label
     * @param null $column
     * @return \Ublaboo\DataGrid\Column\ColumnNumber
     */
    public function addColumnNumber($key, $name, $column = null)
    {
        $column = parent::addColumnNumber($key, $name, $column);
        $column->setSortable();

        return $column;
    }

    /**
     * Makes every column sortable by default
     *
     * @param string $key : column name
     * @param string $name : filter label
     * @param null $column
     * @return \Ublaboo\DataGrid\Column\ColumnDateTime
     */
    public function addColumnDateTime($key, $name, $column = null)
    {
        $column = parent::addColumnDateTime($key, $name, $column);
        $column->setSortable();

        return $column;
    }

    /**
     * Allows filtering by parts of text using LIKE sql command
     *
     * @param string $key : column name
     * @param string $name : filter label
     * @param null $columns
     * @return FilterText
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function addFilterText($key, $name, $columns = null)
    {
        $filterText = parent::addFilterText($key, $name, $columns);

        $filter = parent::getFilter($key);

        $filter->setCondition(function ($selection, $value) use ($key)
        {
            /** @var Selection $selection */
            $selection->where($key . ' LIKE', '%' . $value . '%');
        });

        return $filterText;
    }
}