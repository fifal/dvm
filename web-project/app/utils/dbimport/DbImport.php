<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 28.5.18
 * Time: 22:01
 */

namespace DVM;


use App\Model\DnaModel;
use App\Model\EmployeeModel;
use App\Model\PanelGeneModel;
use App\Model\PatientModel;
use App\Model\SampleTypeModel;
use App\Model\VariantModel;
use PDO;

class DbImport
{
    use \Nette\SmartObject;

    /** @var \Nette\Database\Context */
    private $context;

    private $filePath;
    private $dbFileConnection;
    
    /** @var VariantModel @inject */
    public $variantModel;
    /** @var EmployeeModel @inject */
    public $employeeModel;
    /** @var DnaModel @inject */
    public $dnaModel;
    /** @var PatientModel @inject */
    public $patientModel;

    public function __construct(\Nette\Database\Context $context,
                                VariantModel $variantModel,
                                EmployeeModel $employeeModel,
                                DnaModel $dnaModel,
                                PatientModel $patientModel)
    {
        $this->context = $context;
        $this->variantModel = $variantModel;
        $this->employeeModel = $employeeModel;
        $this->dnaModel = $dnaModel;
        $this->patientModel = $patientModel;
    }

    /**
     * Sets path to DB file and creates connection
     *
     * @param $filepath
     */
    public function setFileConnection($filePath)
    {
        $this->filePath = $filePath;

        $dsn = 'odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=' . $this->filePath . ';CHARSET=utf8';
        $user = '';
        $password = '';

        $this->dbFileConnection = new \PDO($dsn, $user, $password);
        // TODO: WTF NEJDE :D
//        $this->dbFileConnection->exec("SET NAMES utf8");
//        $this->dbFileConnection->exec('SET CHARACTER SET utf8');
//        $this->dbFileConnection->exec('SET SESSION collation_connection = utf8_general_ci');
        $this->dbFileConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Imports table from database file into MSSQL database based on table type
     *
     * @param $tableName : table name
     * @param $type : table type
     */
    public function importType($tableName, $type)
    {
        // Určitě nebudeme potřebovat všechny switche, protože nějaký typy tabulek tam vůbec nemají,
        //      ale tak pro jistotu zatim, jedeme :D

        switch ($type) {
            case DbImportTableEnums::DNA:
                {
                    break;
                }
            case DbImportTableEnums::EMPLOYEE:
                {
                    $this->importVysetrujici($tableName);
                    break;
                }
            case DbImportTableEnums::EXAMINATION:
                {
                    break;
                }
            case DbImportTableEnums::GENE:
                {
                    break;
                }
            case DbImportTableEnums::PANEL:
                {
                    break;
                }
            case DbImportTableEnums::PANEL_GENE:
                {
                    break;
                }
            case DbImportTableEnums::PATIENT:
                {
                    $this->importPatient($tableName);
                    break;
                }
            case DbImportTableEnums::SAMPLE_TYPE:
                {
                    $this->importSampleType($tableName);
                    break;
                }
            case DbImportTableEnums::VARIANT:
                {
                    $this->importVariant($tableName);
                    break;
                }
            case DbImportTableEnums::VARIANT_EXAMINATION:
                {
                    break;
                }
        }
    }

    /**
     * Imports patient into database
     *
     * @param $tableName : old patient table name (u nás tabulka DNA v DNAnova_vzorZCU.mdb)
     */
    //TODO: CIS_VZ přidat, kam půjde... plus PROV, MAT, DAT to už bude někde ve vyšetření
    // Pacient teoreticky pro každý číslo vzorku přidat do tabulky dna novej záznam
    //          -> dna_id auto
    //          -> dna_typ_vzorku_id = MAT
    //          -> dna_pacient_id = nově vloženej pacient (získat kurva id novýho záznamu)
    //          -> dna_datum = DAT
    //TODO: Asi funguje, potřebuje testování + ošetření
    //TODO: Žluťoučký kůň úpěl -> Žlutoucký kun úpel
    private function importPatient($tableName)
    {
        $patientColumns = array(
            'PRIJMENI' => 'pacient_prijmeni',
            'JMENO' => 'pacient_jmeno',
        );
        $patientRc = array(
            'RC1',
            'RC2'
        );
        $patientRcColumnName = 'pacient_rc';
        $dnaColumns = array(
            'DAT' => 'dna_datum',
            'MAT' => 'dna_typ_vzorku_id',
            'CISVZ' => 'dna_cislo_vzorku'
        );
        $dnaPatientColumnName = 'dna_pacient_id';

        $results = $this->dbFileConnection->query('SELECT * FROM ' . $tableName)->fetchAll();

        foreach ($results as $result) {
            $patientData = array();
            $dnaData = array();

            // Finds Name and Surname
            foreach ($patientColumns as $columnOld => $columnNew) {
                $value = iconv('', 'utf-8', $result[$columnOld]);
                $patientData[$columnNew] = $value;
            }

            // Adds RC1 and RC2 together
            $rcNew = '';
            foreach ($patientRc as $rc) {
                $rcNew .= $result[$rc];
            }

            // Add RC to data
            $patientData[$patientRcColumnName] = $rcNew;

            // Insert patient
            $this->patientModel->insertPatient($patientData);

            // Last inserted ID into pacient table
            $patientId = $this->context->getInsertId('pacient');

            // Patient ID in dna table
            $dnaData[$dnaPatientColumnName] = $patientId;
            foreach ($dnaColumns as $columnOld => $columnNew) {
                $dnaData[$columnNew] = $result[$columnOld];
            }

            // Insert data to DNA table
            $this->dnaModel->insertDna($dnaData);
        }
    }

    /**
     * Imports variant into database
     *
     * @param $tableName : old variant table name (u nás třeba tabulka BRCA v BRCA_vzorZCU.accdb)
     */
    private function importVariant($tableName)
    {
        $variantColumns = array(
            'cDNA' => 'varianta_hgvs',
            'RS' => 'varianta_rs',
            'AKTU' => 'varianta_clinvar_datum',
            'PROT' => 'varianta_clinvar_protein',
            'Poznámky' => 'varianta_poznamka'
        );
        
        $results = $this->dbFileConnection->query('SELECT * FROM ' . $tableName)->fetchAll();
        foreach ($results as $result) {
            $data = array();
            foreach ($variantColumns as $columnOld => $columnNew) {

                // For columns with UTF-8 chars in name
                $value = iconv('', 'utf-8', $result[utf8_decode($columnOld)]);

                // For all RS columns, parse just number
                if($columnOld == 'RS'){
                    $value = (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                    $value = str_replace(array('+','-'), '', $value);
                }

                $data[$columnNew] = $value;
            }
            $this->variantModel->insertVariant($data);
        }
    }

    /**
     * Imports employees into database
     *
     * @param $tableName : old table name (u nás PROV v DNAnova_vzorZCU.mdb)
     */
    private function importVysetrujici($tableName)
    {
        /**
         * In old table it's just ID, JMENO
         */
        $nameColumn = 'JMENO';

        $results = $this->dbFileConnection->query('SELECT ' . $nameColumn . ' FROM ' . $tableName)->fetchAll();
        foreach ($results as $result) {
            $value = iconv('', 'utf-8', $result[$nameColumn]);

            $data = $this->parseNameForEmployeeTable($value);
            $this->employeeModel->insertEmployee($data);
        }
    }

    /**
     * Parses name from formats into associative array:
     *      A. BBBBBBBB
     *      Ing. A. BBBBBBBB, Ph.D.
     *      Ing. C. DDDDDDDDD
     *      Mgr. R. EEEEEEEEE, Ph.D. Csc
     *      Mgr. Bc. S. EEEEEEEEE GGGGGGGG, Ph.D. Csc
     *
     * @param $input : input string one of supported formats
     * @return array: associative array for table dbo.vysetrujici
     */
    private function parseNameForEmployeeTable($input)
    {
        // If count is 2 (split by ,) => there is degree after name => assign to $degreeAfter.
        //      and the rest before "," assign to $nameBeforeComma

        // Example for input: Mgr. Bc. S. EEEEEEEEE GGGGGGGG, Ph.D. Csc
        $after = explode(',', $input);
        if (count($after) == 2) {
            $degreeAfter = trim($after[1]);
            $nameBeforeComma = $after[0];
        } else {
            $degreeAfter = '';
            $nameBeforeComma = $input;
        }

        // Explode name by "."
        $degreeBefore = '';

        // Example for $nameBeforeComma: Mgr. Bc. S. EEEEEEEEE GGGGGGGG
        $before = explode('.', $nameBeforeComma);
        $splitCount = count($before);

        // Number of degrees is equal to split array count - 2 => assign to $degreeBefore
        for ($i = 0; $i < $splitCount - 2; $i++) {
            $degreeBefore .= $before[$i] . '.';
        }

        // Rest of the name starts at $splitCount - 2
        $fullName = '';
        for ($i = $splitCount - 2; $i < $splitCount; $i++) {
            $fullName .= $before[$i];
        }

        // Example for $fullName = S EEEEEEEEE GGGGGGGG
        $fullName = explode(' ', trim($fullName));

        // First one is always name, the rest is surname
        $name = $fullName[0];
        $surname = '';
        for ($i = 1; $i < count($fullName); $i++) {
            $surname .= ' ' . $fullName[$i];
        }

        // Delete whitespace from beginning and end of a string
        $surname = trim($surname);

        $data = array(
            'vysetrujici_jmeno' => $name,
            'vysetrujici_prijmeni' => $surname,
            'vysetrujici_titul_pred' => $degreeBefore,
            'vysetrujici_titul_za' => $degreeAfter,
            'vysetrujici_role' => null
        );

        return $data;
    }

    /**
     * Imports sample type into database
     *
     * @param $tableName : name of table with samples
     */
    private function importSampleType($tableName)
    {
        // In DNAnova_vzorZCU tabulka MAT sloupec jméno
        $nameColumn = 'JMENO';
        $idColumn = 'MAT';

        $results = $this->dbFileConnection->query('SELECT * FROM ' . $tableName)->fetchAll();
        $sampleModel = new SampleTypeModel($this->context);


        foreach ($results as $result) {
            $data = array();

            $data['typ_vzorku_id'] = $result[$idColumn];

            $value = iconv('', 'utf-8', $result[$nameColumn]);
            $data['typ_vzorku_nazev'] = $value;

            $sampleModel->insertSampleType($data);
        }
    }

    /**
     * Returns table names in array
     *
     * @return array
     */
    public function getTableNames()
    {
        $dsn = 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=' . $this->filePath . ';CHARSET=UTF-8';
        $user = '';
        $password = '';

        $db = odbc_connect($dsn, $user, $password);

        $tables = array();
        $tblRow = 1;
        $result = odbc_tables($db);
        while (odbc_fetch_row($result)) {
            if (odbc_result($result, "TABLE_TYPE") == "TABLE") {
                array_push($tables, utf8_encode(odbc_result($result, "TABLE_NAME")));
                $tblRow++;
            }
        }

        return $tables;
    }

    /**
     * Returns column names for each table in associative array
     *
     * @return array: array(DNA => array(columns), BRCA => array(columns)
     */
    public function getColumnNames()
    {
        $dsn = 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=' . $this->filePath . ';CHARSET=UTF-8';
        $user = '';
        $password = '';

        $db = odbc_connect($dsn, $user, $password);

        $tableColumns = array();

        $result = odbc_tables($db);
        while (odbc_fetch_row($result)) {
            if (odbc_result($result, "TABLE_TYPE") == "TABLE") {
                $tableName = odbc_result($result, "TABLE_NAME");
                $cols = odbc_exec($db, "SELECT * FROM $tableName WHERE 1=2");
                $ncols = odbc_num_fields($cols);

                $columns = array();
                for ($n = 1; $n <= $ncols; $n++) {
                    array_push($columns, utf8_encode(odbc_field_name($cols, $n)));
                }

                $tableColumns[$tableName] = $columns;
            }
        }
        return $tableColumns;
    }
}