<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 08.06.2018
 * Time: 21:57
 */

namespace DVM;

/**
 * Enum with array that contains types of tables from new database.
 *
 * Old database files has multiple tables in it. This enum contains all possible types of new tables,
 * so user can easily choose what kind of table will the old one be in the new one.
 *
 * Class DbImportTableEnums
 * @package DVM
 */
abstract class DbImportTableEnums
{
    const DONT_IMPORT = 'NOIMPORT';
    const DNA = 'DNA'; // alias DnaModel (dbo.dna)
    const GENE = 'GEN'; // alias GeneModel (dbo.gen)
    const PATIENT = 'PACIENT'; // alias PatientModel (dbo.patient)
    const PANEL = 'PANEL'; // alias PanelModel (dbo.panel)
    const PANEL_GENE = 'PANEL_GEN'; // alias PanelGeneModel (dbo.panel_gen)
    const SAMPLE_TYPE = 'TYP_VZORKU'; // alias SampleTypeModel (dbo.typ_vzorku)
    const VARIANT = 'VARIANTA'; // alias VariantModel (dbo.varianta)
    const VARIANT_EXAMINATION = 'VARIANTA_VYSETRENI'; // alias VariantExamModel (dbo.varianta_vysetreni)
    const EXAMINATION = 'VYSETRENI'; // alias ExaminationModel (dbo.vysetreni)
    const EMPLOYEE = 'VYSETRUJICI'; // alias EmployeeModel (dbo.vysetrujici)

    public static function getEnum($string)
    {
        switch ($string) {
            case 'NOIMPORT':
                {
                    return DbImportTableEnums::DONT_IMPORT;
                }
            case 'DNA':
                {
                    return DbImportTableEnums::DNA;
                }
            case 'GEN':
                {
                    return DbImportTableEnums::GENE;
                }
            case 'PACIENT':
                {
                    return DbImportTableEnums::PATIENT;
                }
            case 'PANEL':
                {
                    return DbImportTableEnums::PANEL;
                }
            case 'PANEL_GEN':
                {
                    return DbImportTableEnums::PANEL_GENE;
                }
            case 'TYP_VZORKU':
                {
                    return DbImportTableEnums::SAMPLE_TYPE;
                }
            case 'VARIANTA':
                {
                    return DbImportTableEnums::VARIANT;
                }
            case 'VARIANTA_VYSETRENI':
                {
                    return DbImportTableEnums::VARIANT_EXAMINATION;
                }
            case 'VYSETRENI':
                {
                    return DbImportTableEnums::EXAMINATION;
                }
            case 'VYSETRUJICI':
                {
                    return DbImportTableEnums::EMPLOYEE;
                }
        }
    }
}