<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 11.06.2018
 * Time: 18:39
 *
 * Class provides moveFile function which moves file to temp dir on server (www/tmp)
 */

class FilesStorage
{
    use \Nette\SmartObject;

    private $dir;

    public function __construct($fileDir)
    {
        $this->dir = str_replace('\\', '/', $fileDir);
    }

    /**
     * Moves file to temporary dir on server (www/tmp)
     * @param $file: file to move
     */
    public function moveFile($file){
        $file->move($this->dir . $file->getName());
    }

    /**
     * Returns absolute path to www/tmp
     *
     * @return mixed
     */
    public function getPath(){
        return $this->dir;
    }
}