<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 26.5.18
 * Time: 18:20
 *
 * Přepravka pro informace o genech
 */

namespace App\Utils\Varsome;


class VarsomeGene
{
    use \Nette\SmartObject;

    private $symbol; //BRCA1, BRAF, atd...
    private $geneId; //2277
    private $description; // Breast cancer...
    private $synonyms; //BRCA1 - BRCC1, FANCS, PPP1R53...
    private $reference; //LRG_292

    public function __construct()
    {
        $this->symbol = null;
        $this->geneId = null;
        $this->description = null;
        $this->synonyms = null;
        $this->reference = null;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return null
     */
    public function getGeneId()
    {
        return $this->geneId;
    }

    /**
     * @param null $geneId
     */
    public function setGeneId($geneId)
    {
        $this->geneId = $geneId;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getSynonyms()
    {
        return $this->synonyms;
    }

    /**
     * @param null $synonyms
     */
    public function setSynonyms($synonyms)
    {
        $this->synonyms = $synonyms;
    }

    /**
     * @return null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param null $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }



    public function toString(){
        return 'symbol: ' . $this->symbol
            . ', geneId: ' . $this->geneId
            . ', descritption: ' . $this->description;
    }


}