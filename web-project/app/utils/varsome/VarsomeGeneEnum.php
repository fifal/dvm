<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 24.5.18
 * Time: 23:33
 *
 * Enum s možnýma zdrojovýma databázema, při používání getGeneLookup()
 */

namespace App\Utils\Varsome;


abstract class VarsomeGeneEnum
{
    const ALL = 'all';
    const NONE = 'none';
    const DGI = 'dgi';
    const CGD= 'cgd';
    const GHR_GENES = 'ghr-genes';
    const DBNSFP_GENES = 'dbnsfp-genes';
    const CIVIC = 'civic';
    const EXAC_GENES = 'exac-genes';
}