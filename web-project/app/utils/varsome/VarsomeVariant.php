<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 25.5.18
 * Time: 13:00
 *
 * Přepravka pro varianty
 */

namespace App\Utils\Varsome;


use Nette\Utils\DateTime;

class VarsomeVariant
{
    use \Nette\SmartObject;

    /** @var string */
    private $rsid; // RS id varianty
    /** @var string */
    private $hgvs;
    /** @var DateTime */
    private $lastEvalDate; // poslední přehodnocení
    /** @var string */
    private $clinvarVerdict; // verdikt
    /** @var int */
    private $clinvarVerdictStars; // počet hvězdiček hodnocení verdiktu

    public function __construct()
    {
        $this->rsid = null;
        $this->lastEvalDate = null;
        $this->clinvarVerdict = null;
        $this->clinvarVerdictStars = null;
    }

    /**
     * @return array
     */
    public function getRsid()
    {
        is_array($this->rsid) ? $result = $this->rsid[0] : $result = $this->rsid;

        return $result;
    }

    /**
     * @param array $rsid
     */
    public function setRsid($rsid)
    {
        $this->rsid = $rsid;
    }

    /**
     * @return string
     */
    public function getHgvs()
    {
        return $this->hgvs;
    }

    /**
     * @param $hgvs
     */
    public function setHgvs($hgvs)
    {
        $this->hgvs = $hgvs;
    }

    /**
     * @return null
     */
    public function getLastEvalDate()
    {
        if(!$this->lastEvalDate instanceof DateTime)
        {
            $this->lastEvalDate = new DateTime($this->lastEvalDate);
        }
        return $this->lastEvalDate;
    }

    /**
     * @param null $lastEvalDate
     */
    public function setLastEvalDate($lastEvalDate)
    {
        $this->lastEvalDate =  $lastEvalDate;
    }

    /**
     * @return null
     */
    public function getClinvarVerdict()
    {
        return $this->clinvarVerdict;
    }

    /**
     * @param null $clinvarVerdict
     */
    public function setClinvarVerdict($clinvarVerdict)
    {
        /*
         * JSON vrací výsledek typu "likely benign **3**"
         */

        // nalezení pozice první *
        $pos = strpos($clinvarVerdict, '*');

        // substring verdict "likely benign"
        $verdict = substr($clinvarVerdict, 0, $pos - 1);

        // substring hodnocení "3"
        $stars = substr($clinvarVerdict, $pos + 2, 1);

        // nastavení verdiktu a hvězdiček
        $this->clinvarVerdict = $verdict;
        $this->setClinvarVerdictStars($stars);
    }

    /**
     * @return null
     */
    public function getClinvarVerdictStars()
    {
        return $this->clinvarVerdictStars;
    }

    /**
     * @param null $clinvarVerdictStars
     */
    public function setClinvarVerdictStars($clinvarVerdictStars)
    {
        $this->clinvarVerdictStars = $clinvarVerdictStars;
    }

    public function toString()
    {
        $rsid = is_array($this->getRsid()) ? $this->getRsid()[0] : $this->getRsid();

        return 'rs: ' . $rsid
            . ', clivarVerdcit: ' . $this->clinvarVerdict
            . ', clinvarStars: ' . $this->clinvarVerdictStars
            . ', lastEval: ' . $this->lastEvalDate;
    }

}