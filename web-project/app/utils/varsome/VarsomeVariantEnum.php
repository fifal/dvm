<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 24.5.18
 * Time: 19:06
 *
 * Enum pro všechny možný zdrojový databáze při používání getVariantLookup()
 */

namespace App\Utils\Varsome;


abstract class VarsomeVariantEnum
{
    const ALL = 'all';
    const NONE = 'none';
    const NCBI_DBSNP = 'ncbi-dbsnp';                            // The Single Nucleotide Polymorphism database
    const DBNSFP = 'dbnsfp';                                    // Functional prediction and annotation of
                                                                // all potential non-synonymous single-nucleotide variants
    const WUSTL_CIVIC = 'wustl-civic';
    const SANGER_COSMIC_LICENSED = 'sanger-cosmic-licensed';    // Catalogue Of Somatic Mutations In Cancer
    const IARC_TP53_GERMLINE = 'iarc-tp53-germline';            // Knowledgebase and statistical tools for the analysis
                                                                // of TP53 gene mutations in human
    const REFSEQ_TRANSCRIPTS = 'refseq-transcripts';            // NCBI Reference Sequence Database
    const GERP = 'gerp';                                        // Genomic Evolutionary Rate Profiling
    const ICGC_SOMATIC = 'icgc-somatic';                        // Simple Somatic Mutation Format
    const ENSEMBL_TRANSCIPTS = 'ensembl-transcripts';           // Gene annotation in Ensembl
    const GNOMAD_EXOMES_COVERAGE = 'gnomad-exomes-coverage';    // Genome Aggregation Database
    const IARC_TP53_SOMATIC = 'iarc-tp53-somatic';              // Knowledgebase and statistical tools for the analysis
    const ISB_KAVIAR3 = 'isb-kaviar3';                          // Kaviar Genomic Variant Database
    const SANGER_COSMIC_PUBLIC = 'sanger-cosmic-public';        // Catalogue Of Somatic Mutations In Cancer
    const GWAS = 'gwas';                                        // Genome-wide association study
    const DANN_SNVS = 'dann-snvs';                              //
    const DBNSFP_DBSCSNV = 'dbnsfp-dbscsnv';                    //
    const THOUSAND_GENOMES = 'thousand-genomes';                // International Genome Sample Resource
    const GNOMAD_GENOMES_COVERAGE = 'gnomad-genomes-coverage';  //
    const GNOMAD_GENOMES = 'gnomad-genomes';                    //
    const BROAD_EXAC = 'broad-exac';                            // The Exome Aggregation Consortium
    const NCBI_CLINVAR2 = 'ncbi-clinvar2';                      // ClinVar aggregates information about genomic variation
    const GNOMAD_EXOMES = 'gnomad-exomes';                      //
    const SANGER_COSMIC = 'sanger-cosmic';                      //
    const UNIPROT_VARIANTS = 'uniprot-variants';                // Resource of protein sequence and functional information.
}